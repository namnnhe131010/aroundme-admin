import { combineReducers } from "redux";
import { all, fork } from "redux-saga/effects";
import { authReducer, authSaga } from "./modules/auth";

export function* rootSagas() {
  yield all([fork(authSaga)]);
}

export const rootReducers = combineReducers({
  authReducer,
});
