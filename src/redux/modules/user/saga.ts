import { Action } from "interfaces/redux";
import { takeLatest, put } from "redux-saga/effects";
import * as types from "redux/types";
import { getUserSuccess } from "./actions";
import _ from "lodash";
import { onSagaSuccess } from "redux/reduxHelpers";
import userServices from "services/userServices";

function* getUser(action: Action) {
  try {
    const response = yield userServices.getUser();
    // console.log("user here", response);
    onSagaSuccess(action, response, yield put(getUserSuccess(response.data)));
    window.localStorage.setItem("userType", JSON.stringify(response.data));

  } catch (error) {
    yield put({ type: types.GET_ACCOUNT_FAILED, error });
  }
}

export function* userSaga() {
  yield takeLatest(types.GET_ACCOUNT, getUser);
}
