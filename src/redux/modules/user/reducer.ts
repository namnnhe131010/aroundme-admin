import ReducerInterface from "interfaces/reducerInterface";
import produce from "immer";
import * as types from "redux/types";

const initialState = {
  dataUser: {
    data: {},
    error: null,
    loading: false,
  },
  types: "",
};

export const userReducer = (state = initialState, action: ReducerInterface) => {
  return produce(state, (draftState) => {
    switch (action.type) {
      // get user
      case types.GET_ACCOUNT:
        draftState.dataUser.loading = false;
        break;

      case types.GET_ACCOUNT_SUCCESS:
        draftState.dataUser.loading = false;
        draftState.dataUser.data = action.payload;
        break;

      case types.GET_ACCOUNT_FAILED:
        draftState.dataUser.loading = false;
        draftState.dataUser.error = action.error;
        break;
      case types.REQUEST_LOGOUT:
        draftState.dataUser.data = {};
        break;
      default:
        break;
    }
  });
};
