import { ReduxCallbacks } from "interfaces/redux";
import * as types from "redux/types";

// get user

export const getUserRequest = (callbacks?: ReduxCallbacks) => ({
  type: types.GET_ACCOUNT,
  payload: { callbacks },
});

export const getUserSuccess = (payload: any) => ({
  type: types.GET_ACCOUNT_SUCCESS,
  payload,
});

export const getUserFailed = (payload: any) => ({
  type: types.GET_ACCOUNT_FAILED,
  payload,
});
