import { appToast } from "components/AppToastContainer";
import { Action } from "interfaces/redux";
import { put, takeLatest } from "redux-saga/effects";
import { dmChucNangMenu } from "redux/modules/menus";
import { onSagaSuccess } from "redux/reduxHelpers";
import * as types from "redux/types";
import { attachTokenToHeader, saveLocalStorage } from "services/httpService";
import loginServices from "services/loginServices";
import { loginResquestSuccess } from "./actions";

const urlPath = window.location.href;

function* login(action: Action): any {
  const { ...remainParams } = action.payload;

  try {
    const response = yield loginServices.login(remainParams);
    // debugger
    if(response?.status === 200 && response?.data?.token) {
      saveLocalStorage(response?.data?.token);
      attachTokenToHeader(response?.data?.token);
      onSagaSuccess(action, response, yield put(loginResquestSuccess(response.data)));
    }else{
      yield put({ type: types.REQUEST_LOGIN_FAILED, payload: "Errors" });
    }
    action.loading(false);
  } catch (error) {
    console.log(error.response);
    // debugger
    action.loading(false);
    yield put({ type: types.REQUEST_LOGIN_FAILED, error });
    appToast({ toastOptions: { type: "error" } });
  }
}

function* loutOut() {
  yield localStorage.removeItem("user");
  yield localStorage.removeItem("userType");
  // window.location.reload();
}

function* getMenu(): any {
  try {
    const response = yield dmChucNangMenu();
    const data = response.data.data;
    yield put({ type: types.REQUEST_MENU_SUCCESS, payload: data });
  } catch (error) {
    yield put({ type: types.REQUEST_MENU_FAIL, error });
    appToast({ toastOptions: { type: "error" } });
  }
}

export function* authSaga() {
  yield takeLatest(types.REQUEST_LOGIN, login);
  yield takeLatest(types.REQUEST_LOGOUT, loutOut);
  yield takeLatest(types.REQUEST_MENU, getMenu);
}
