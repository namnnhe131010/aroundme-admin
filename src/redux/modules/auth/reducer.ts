import ReducerInterface from "interfaces/reducerInterface";
import produce from "immer";
import * as types from "redux/types";

const initialState = {
  auth: {
    isLogin: false,
    error: null,
    res: {},
  },
  menu: [],
  types: "",
};

export const authReducer = (state = initialState, action: ReducerInterface) => {
  return produce(state, (draftState) => {
    draftState.types = action.type;
    switch (action.type) {
      case types.REQUEST_LOGIN:
        draftState.auth.isLogin = false;
        break;

      case types.REQUEST_LOGIN_SUCCESS:
        draftState.auth.isLogin = true;
        draftState.auth.res = action.payload;
        break;

      case types.REQUEST_LOGIN_FAILED:
        draftState.auth.isLogin = false;
        draftState.auth.error = action.payload;
        break;

      case types.REQUEST_LOGOUT:
        draftState.auth.isLogin = false;
        break;

      case types.REQUEST_MENU:
        draftState.menu = [];
        break;

      case types.REQUEST_MENU_SUCCESS:
        draftState.menu = action?.payload;
        break;

      case types.REQUEST_MENU_FAIL:
        draftState.menu = [];
        break;

      default:
        break;
    }
  });
};
