import { CHANGE_PASSWORD } from "constants/api";
import { ReduxCallbacks } from "interfaces/redux";
import * as types from "redux/types";
import httpServices from "services/httpServices";

// get todos list
export const loginRequest = (payload: any, loading: any) => ({
  type: types.REQUEST_LOGIN,
  loading,
  payload,
});
export const resetType = () => ({
  type: types.RESET_TYPE,
});

export const loginResquestSuccess = (payload: any) => ({
  type: types.REQUEST_LOGIN_SUCCESS,
  payload,
});

export const loginResquestFailed = (payload: any) => ({
  type: types.REQUEST_LOGIN_FAILED,
  payload,
});

export const changePWRequest = async (payload: { currentPassword: string; newPassword: string }) => {
  return await httpServices.post(CHANGE_PASSWORD, payload);
};

export const logOutRequest = () => ({
  type: types.REQUEST_LOGOUT,
});

// Menu
export const getMenuRequest = () => ({
  type: types.REQUEST_MENU,
});
export const getMenuRequestSuccess = (payload: any) => ({
  type: types.REQUEST_MENU_SUCCESS,
  payload,
});
export const getMenuRequestFailed = (payload: any) => ({
  type: types.REQUEST_MENU_FAIL,
  payload,
});
