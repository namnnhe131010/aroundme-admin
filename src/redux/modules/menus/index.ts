import { MENU } from "constants/api";
import httpServices from "services/httpServices";


export const dmChucNangMenu = async () => {
  return await httpServices.post(`${MENU.MENU_CHUC_NANG}`);
};