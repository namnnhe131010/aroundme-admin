import { useSelector } from "react-redux";

export const GetUserSelector = () => {
  const dataUser = useSelector((state: any) => state.userReducer.dataUser);
  if (dataUser) {
    return dataUser;
  }
};
