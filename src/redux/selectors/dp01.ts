import { useSelector } from "react-redux";

export const GetDP01Selector = () => {
  const listVerifyResult = useSelector((state: any) => state.dp01Reducer.listVerifyResult);
  if (listVerifyResult) {
    return listVerifyResult;
  }
};
