import { useSelector } from "react-redux";

export const GetAuthSelector = () => {
  const auth = useSelector((state: any) => state.authReducer.auth);
  if (auth) {
    // console.log("GetAuthSelector -> auth", auth);
    return auth;
  }
  return null;
};

export const GetMenu = () => {
  const menu = useSelector((state: any) => state.authReducer.auth.res.danhSachChucNang);
  if (menu) {
    // console.log("GetAuthSelector -> auth", menu);
    return menu;
  }
};

export const GetUserInfo = () => {
  const data = useSelector((state: any) => state.userReducer.dataUser.data);
  if (data) {
    // console.log("GetAuthSelector -> auth", data);
    return data;
  }
};

export const GetTypeAuth = () => {
  const types = useSelector((state: any) => state.authReducer.types);
  if (types) {
    // console.log("GetAuthSelector -> auth", types);
    return types;
  }
};
