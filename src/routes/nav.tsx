export default {
  MENU_DP: [
    // 1. Tra Cứu
    {
      name: "Tra cứu",
      url: "/tra-cuu",
      routes: [
        {
          name: "Tra cứu tình hình xử lý yêu cầu cấp CCCD",
          url: "/tinh-hinh-xu-ly-yeu-cau-cap-cccd",
        },
        {
          name: "Tra cứu thông tin cá nhân",
          url: "/thong-tin-ca-nhan-2",
        },
        {
          name: "Tra cứu thông tin yêu cầu cấp CCCD",
          url: "/thong-tin-yeu-cau-cap-cccd",
        },
        {
          name: "Theo dõi quá trình xử lý hồ sơ",
          url: "/theo-doi-qua-trinh-xu-ly-ho-so",
        },
        {
          name: "Theo dõi tình hình chuyển hồ sơ",
          url: "/trung-uong/theo-doi-tinh-hinh-chuyen-ho-so",
        },
      ],
    },

    // 2. Xử lý hồ sơ
    {
      name: "Xử lý hồ sơ",
      url: "/xu-ly-ho-so",
      routes: [
        {
          name: "Xem và xác nhận thông tin trên thẻ",
          url: "/xem-va-xac-nhan-tt-the",
        },
        {
          name: "Sửa yêu cầu cấp CCCD",
          url: "/sua-yeu-cau-cap-cccd",
        },
        {
          name: "Xử lý hồ sơ đang chờ xử lý thu nhận",
          url: "/ho-so-dang-cho-xy-ly-khi-thu-nhan",
        },
        {
          name: "Xử lý hồ sơ nghi trùng",
          url: "/xu-ly-ho-so-nghi-trung",
        },
        {
          name: "Xử lý hồ sơ vi phạm",
          url: "/xu-ly-trung-ho-so-vi-pham",
        },
        {
          name: "Xử lý bản ghi thao tác bởi người dùng khác",
          url: "/xoa-ban-ghi",
        },
        {
          name: "Xử lý kết quả kiểm tra thông tin",
          url: "/trung-uong/xu-ly-ket-qua-kiem-tra-thong-tin",
        },
        {
          name: "Tạo yêu cầu xoá vân tay trong hệ thông ABIS",
          url: "/trung-uong/tao-yeu-cau-xoa-van-tay-abis",
        },
        {
          name: "Gỡ số CCCD",
          url: "/trung-uong/go-so-cccd",
        },
        
      ],
    },
    // 3. Quản lý danh sách
    {
      name: "Quản lý danh sách",
      url: "/quan-ly-danh-sach",
      routes: [
        {
          name: "Lập danh sách xác minh",
          url: "/lap-danh-sach-ho-so-can-xac-minh",
        },
        {
          name: "Ghi nhận kết quả xác minh",
          url: "/ghi-nhan-ket-qua-xac-minh",
        },
        {
          name: "Lập danh sách yêu cầu cấp CCCD",
          url: "/lap-danh-sach-yeu-cau-cap-CCCD",
        },
        {
          name: "Ghi nhận kết quả phê duyệt",
          url: "/ghi-nhan-ket-qua-phe-duyet",
        },
        {
          name: "In danh sách đề xuất duyệt cấp",
          url: "/in-danh-sach-de-nghi-duyet-cap-cccd-dp",
        },
        {
          name: "In đề nghị xác minh thông tin hồ sơ",
          url: "/in-de-nghi-xac-minh-thong-tin-ho-so",
        },
        {
          name: "Ghi kết quả xác minh",
          url: "/ghi-ket-qua-xac-minh",
        },
        {
          name: "Tạo yêu cầu cấp CCCD",
          url: "/tao-yeu-cau-cap-cccd",
        },
        {
          name: "Chuẩn bị dữ liệu cá thể hóa",
          url: "/trung-uong/chuan-bi-du-lieu-ca-the-hoa",
        },
        {
          name: "Trả kết quả xử lý yêu cầu cấp CCCD",
          url: "/trung-uong/tra-ket-qua-xu-ly-yeu-cau-cap-cccd",
        },
      ],
    },
    // 4. Báo cáo
    {
      name: "Báo cáo",
      url: "/bao-cao",
      routes: [
        {
          name: "Tình hình đăng ký cấp CCCD/CMND theo địa bàn,dân tộc,tôn giáo",
          url: "/dia-phuong-bao-cao/tinh-hinh-dang-ky-cap-CCCD-theo-dia-ban-dan-toc-ton-giao",
        },
        {
          name: "Tình hình đăng ký cấp CCCD/CMND theo tuyến",
          url: "/dia-phuong-bao-cao/tinh-hinh-dang-ky-cap-CCCD-theo-tuyen",
        },
        {
          name: "CCCD đã cấp theo địa bàn, dân tộc, tôn giáo",
          url: "/dia-phuong-bao-cao/CCCD-da-cap-theo-dia-ban-dan-toc-ton-giao",
        },
        {
          name: "CCCD đã cấp theo tuyến",
          url: "/dia-phuong-bao-cao/CCCD-da-cap-theo-tuyen",
        },
        {
          name: "Thống kê danh sách công dân đến hạn đổi CCCD/CMND theo địa bàn, dân tộc, tôn giáo",
          url: "/dia-phuong-bao-cao/ds-cong-dan-den-han-doi-cccd-theo-dia-ban-dan-toc-ton-giao",
        },
        {
          name: "Tình hình công dân đến hạn đổi CCCD/CMND theo tuyến",
          url: "/dia-phuong-bao-cao/tinh-hinh-cong-dan-den-han-doi-cccd-theo-tuyen",
        },
        {
          name: "CCCD/ CMND đã cấp nhưng công dân chưa đến nhận theo địa bàn, dân tộc, tôn giáo",
          url: "/dia-phuong-bao-cao/cccd-da-cap-nhung-cong-dan-chua-den-nhan-theo-dia-ban-dan-toc-ton-giao",
        },
        {
          name: "Thống kê danh sách công dân quá hạn theo địa bàn, dân tộc, tôn giáo",
          url: "/dia-phuong-bao-cao/ds-cong-dan-qua-han-theo-dia-ban-dan-toc-ton-giao",
        },
        {
          name: "Tình hình nhận hồ sơ yêu cầu cấp CCCD/CMND",
          url: "/dia-phuong-bao-cao/nhan-ho-so-yeu-cau-cap-cccd-cmnd",
        },
        {
          name: "Tình hình gửi hồ sơ yêu cầu cấp CCCD/CMND",
          url: "/dia-phuong-bao-cao/gui-ho-so-yeu-cau-cap-cccd-cmnd",
        },
        {
          name: "Số liệu cấp CCCD/CMND",
          url: "/dia-phuong-bao-cao/bao-cao-so-lieu-cap-cccd-cmnd",
        },
        {
          name: "Danh sách hồ sơ CCCD/CMND không được cấp",
          url: "/dia-phuong-bao-cao/danh-sach-ho-so-cccd-cmnd-khong-duoc-cap",
        },
        {
          name: "Tình hình xử lý hồ sơ đề nghị cấp CCCD/CMND chưa lập danh sách",
          url: "/dia-phuong-bao-cao/tinh-hinh-xu-ly-ho-so-de-nghi-cap-cccd-cmnd",
        },
        {
          name: "Thống kê danh sách hồ sơ theo thời hạn xử lý",
          url: "/dia-phuong-bao-cao/thong-ke-danh-sach-ho-so-theo-thoi-han-xu-ly",
        },
        {
          name: "Thống kê số liệu hồ sơ đã thu nhận",
          url: "/dia-phuong-bao-cao/thong-ke-so-lieu-ho-so-da-thu-nhan",
        },
        {
          name: "Thống kê chi tiết hồ sơ yêu cầu cấp CCCD",
          url: "/dia-phuong-bao-cao/thong-ke-chi-tiet-ho-so-yeu-cau-cap-cccd",
        },
        {
          name: "Thống kê tổng hợp lệ phí cấp CCCD",
          url: "/dia-phuong-bao-cao/thong-ke-tong-hop-le-phi-cap-cccd",
        },
        {
          name: "Thống kê hồ sơ miễn phí theo loại cấp",
          url: "/dia-phuong-bao-cao/thong-ke-ho-so-mien-phi-theo-loai-cap",
        },
        {
          name: "Thống kê lý do hồ sơ miễn phí",
          url: "/dia-phuong-bao-cao/thong-ke-ly-do-ho-so-mien-phi",
        },
        {
          name: "Thống kê hồ sơ cấp cccd theo năm sinh",
          url: "/dia-phuong-bao-cao/thong-ke-ho-so-cap-cccd-theo-nam-sinh",
        },
      ],
    },

    // 5. Hệ thống
    {
      name: "Hệ thống",
      url: "/he-thong",
      routes: [
        {
          name: "Danh sách phòng ban",
          url: "/phong-ban",
        },
        {
          name: "Danh sách cán bộ",
          url: "/can-bo",
        },
        {
          name: "Danh sách nhóm quyền",
          url: "/quyen-va-nhom-quyen",
        },
        {
          name: "Khai báo sử dụng và phân quyền",
          url: "/khai-bao-su-dung-va-phan-quyen",
        },
        {
          name: "Đăng ký trạm thu nhận",
          url: "/dang-ky-tram-thu-nhan",
        },
        // {
        //   name: "Đổi mật khẩu",
        //   url: "/thay-doi-mat-khau",
        // },
        {
          name: "Nhập hồ sơ vi phạm",
          url: "/nhap-ho-so-vi-pham",
        },
        {
          name: "Danh sách địa chính",
          url: "/danh-sach-dia-chinh",
        },
        {
          name: "Danh sách đơn vị trực thuộc",
          url: "/don-vi-truc-thuoc",
        },
        {
          name: "Khai báo danh lục hệ thống",
          url: "/khai-bao-danh-muc-he-thong",
        },
      ],
    },
  ],
};
