import withErrorBoundary from "components/HOCs/withErrorBoundary";
import { lazy } from "react";
import perrmission from "routes/permission";
import Location from 'views/Location';
import User from 'views/User';

// const blankPage = lazy(() => import("views/BlankPage"));
const page404 = lazy(() => import("views/404"));
// const QuanLyBaiViet = lazy(() => import("views/QuanLyBaiViet"));

export default [
  {
    path: "/",
    exact: true,
    name: "Home",
    component: withErrorBoundary(Location),
  },
  {
    path: "/location",
    exact: true,
    name: "Location",
    component: withErrorBoundary(Location),
  },
  // {
  //   path: "/category",
  //   exact: true,
  //   name: "category",
  //   component: withErrorBoundary(Category),
  // },
  {
    path: "/user",
    exact: true,
    name: "category",
    component: withErrorBoundary(User),
  },

  { name: "404", component: withErrorBoundary(page404, perrmission.ALL) },
];
