import httpServices from "./httpServices";
import { LOGIN } from "constants/api";

class LoginServices {
  postLogin(payload: any) {
    return httpServices.post(LOGIN, { ...payload });
  }
}
export default new LoginServices();
