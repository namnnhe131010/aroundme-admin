import axios, { AxiosRequestConfig, AxiosResponse, AxiosError } from "axios";
import { CLIENT_ID, LOGIN_IDP } from "constants/api";
import { getDataToken } from "helpers/getLocalStore";
import moment from "moment";
import cloneDeep from "lodash/cloneDeep";

export const BASE_URL = "http://10.100.131.225:8008/bviet/api";

class Services {
  axios: any;
  interceptors: null;
  refreshTokenRequest: any;

  constructor() {
    this.axios = axios;
    this.interceptors = null;
    this.refreshTokenRequest = null;
    this.axios.defaults.withCredentials = true;
    // this.handleRequestInterceptors();
  }

  handleRequestInterceptors() {
    debugger
    this.axios.interceptors.request.use(
      async (request: AxiosRequestConfig) => {
        if (!getDataToken()) return request;

        const { timeExpired, refresh_token, refreshExpired } = getDataToken();
        const isExpired = moment().isSameOrAfter(moment(timeExpired).subtract(60, "seconds"));
        const isRefreshExpired = moment().isSameOrAfter(refreshExpired);

        if (isExpired) {
          if (isRefreshExpired) {
            this.clearLocalStorage();
          }

          this.refreshTokenRequest = this.refreshTokenRequest
            ? this.refreshTokenRequest
            : this.getRefreshToken(refresh_token);

          const response = await this.refreshTokenRequest;
          // console.log(response, "response");
          if (response.status === 200) {
            const dataToken = {
              ...response?.data,
              timeExpired: moment().add(response?.data?.expires_in, "seconds"),
              refreshExpired: moment().add(response?.data?.refresh_expires_in, "seconds"),
            };

            this.saveLocalStorage(dataToken);
            this.refreshTokenRequest = null;
          } else {
            this.clearLocalStorage();
          }
        }

        const cloneConfig = cloneDeep(request);
        const { access_token } = getDataToken();
        // cloneConfig.headers.common["Accept"] = "application/json";
        cloneConfig.headers.common["Authorization"] = `Bearer ${access_token}`;
        return cloneConfig;
      },
      (err: any) => {}
    );
  }

  test() {
    return this.axios.interceptors;
  }

  getRefreshToken(refresh_token: string) {
    const params = new URLSearchParams();
    params.append("client_id", CLIENT_ID);
    params.append("grant_type", "refresh_token");
    params.append("refresh_token", refresh_token);

    return new Promise((resolve, reject) => {
      axios
        .post(LOGIN_IDP, params, {
          headers: {
            "Content-Type": "application/x-www-form-urlencoded",
          },
        })
        .then((res) => {
          resolve(res);
        })
        .catch((err) => reject(err));
    });
  }

  saveLocalStorage(data: any) {
    window.localStorage.setItem("token", JSON.stringify(data));
  }

  clearLocalStorage() {
    window.localStorage.removeItem("token");
    window.location.reload();
  }

  attachTokenToHeader(token?: string) {
    if (token) {
      this.axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
    } else {
      delete this.axios.defaults.headers.common["Authorization"];
    }
  }

  attachFunctionCodeToHeader(functionCode?: any) {
    this.axios.defaults.headers.common["Function-Code"] = `${functionCode || ""}`;
  }
  removeInterceptors() {
    this.axios.interceptors.request.eject(this.interceptors);
  }

  handleResponse(response: AxiosResponse, error: AxiosError, isSuccess: boolean) {
    if (isSuccess) {
      return response;
    } else {
      if (error.response && error.response.status === 401) {
        // clear token
        // localStorage.removeItem("user");
        // localStorage.removeItem("userType");
        // window.location.reload();
        // return;
      }
      return error.response;
    }
  }

  async get(url: string, config?: AxiosRequestConfig) {
    try {
      const response = await this.axios.get(url, config);
      return this.handleResponse(response, {} as AxiosError, true);
    } catch (error) {
      return this.handleResponse({} as AxiosResponse, error as AxiosError, false);
    }
  }

  async post(url: string, data?: any, config?: AxiosRequestConfig) {
    try {
      const response = await this.axios.post(url, data, config);
      return this.handleResponse(response, {} as AxiosError, true);
    } catch (error) {
      return this.handleResponse({} as AxiosResponse, error as AxiosError, false);
    }
    // return this.axios.post(url, data, config);
  }

  async delete(url: string, config?: AxiosRequestConfig) {
    try {
      const response = await this.axios.delete(url, config);
      return this.handleResponse(response, {} as AxiosError, true);
    } catch (error) {
      return this.handleResponse({} as AxiosResponse, error as AxiosError, false);
    }
    // return this.axios.delete(url, config);
  }

  async put(url: string, data?: any, config?: AxiosRequestConfig) {
    try {
      const response = await this.axios.put(url, data, config);
      return this.handleResponse(response, {} as AxiosError, true);
    } catch (error) {
      return this.handleResponse({} as AxiosResponse, error as AxiosError, false);
    }
  }
}

export const HeaderUTF8Option = {
  headers: {
    "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8",
    "X-HTTP-Method-Override": "GET",
  },
};

export default new Services();
