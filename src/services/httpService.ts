import axios, { AxiosError, AxiosResponse } from "axios";
import cloneDeep from "lodash/cloneDeep";
import { getDataToken } from "helpers/getLocalStore";

export const BASE_URL = "http://172.104.189.141:86/";
// export const BASE_URL = "https://6e9a-171-229-220-6.ngrok.io/";
export const axiosApi = axios.create({
  baseURL: BASE_URL, 
  timeout: 120000,
  headers: {'content-type': 'application/json',
  "Access-Control-Allow-Origin":'*' },
  withCredentials: true,
});

axiosApi.interceptors.request.use(
  async (request) => {
    const cloneConfig = cloneDeep(request);
    const token = getDataToken();
    cloneConfig.headers.common["Authorization"] = `Bearer ${token}`;

    return cloneConfig;
  },
  (error) => {
    return Promise.reject(error);
  }
);

axiosApi.interceptors.response.use(
  (response) => response,
  (error) => {
    return Promise.reject(error);
  }
);

export const saveLocalStorage = (data: any) => {
  window.localStorage.setItem("token", JSON.stringify(data));
};

export const clearLocalStorage = () => {
  window.localStorage.removeItem("token");
  window.location.reload();
};

export const attachTokenToHeader = (token: string) => {
  if (token) {
    axiosApi.defaults.headers.common["Authorization"] = `Bearer ${token}`;
  } else {
    delete axiosApi.defaults.headers.common["Authorization"];
  }
};

const handleResponse = (response: AxiosResponse, error: AxiosError, isSuccess: boolean, url: any) => {
  if (isSuccess) {
    return response;
  } else {
    if (error.response && error.response.status === 401) {
      clearLocalStorage();
      return;
    }
    throw error.response;
  }
};

export async function sendPost(url: string, data: any, config = {}) {
  try {
    const response = await axiosApi.post(url, data, config);
    return handleResponse(response, {} as AxiosError, true, url);
  } catch (error) {
    return handleResponse({} as AxiosResponse, error as AxiosError, false, url);
  }
}

export async function sendGet(url: string, config = {}) {
  try {
    const response = await axiosApi.get(url, config);
    return handleResponse(response, {} as AxiosError, true, url);
  } catch (error) {
    return handleResponse({} as AxiosResponse, error as AxiosError, false, url);
  }
}

export async function del(url: string, config = {}) {
  try {
    const response = await axiosApi.delete(url, { ...config });
    return handleResponse(response, {} as AxiosError, true, url);
  } catch (error) {
    return handleResponse({} as AxiosResponse, error as AxiosError, false, url);
  }
}

export async function put(url: string, data?: any, config={}) {
  try {
    const response = await axiosApi.put(url, data, config);
    return handleResponse(response, {} as AxiosError, true, url);
  } catch (error) {
    return handleResponse({} as AxiosResponse, error as AxiosError, false, url);
  }
}
