import { sendPost } from "./httpService";

class MediaService {
  async convertFileToLinks3 (MediaFile:any, session: string) {
    const formData = new FormData();
    formData.append("file", MediaFile);
    formData.append("session", session);

    try {
        const response = await sendPost(`/api/storage/uploadPostFile`, formData, { "Content-Type": "multipart/form-data" });
        if (response?.status === 200 && response?.data) {
            return response?.data;
        }
    } catch (error) {
        Promise.reject(error)
    }
  }
}

export default new MediaService();
