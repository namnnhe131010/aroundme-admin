import { sendGet } from "./httpService";

class LoginService {
  login(body: {
    username: string, 
    password: string, 
  }) {
    return sendGet(`api/LoginAdmin?phoneNumber=${body.username}&pass=${body.password}`, {
      headers: {"Access-Control-Allow-Origin":'*', 
      "Access-Control-Allow-Credentials": true,
      'Access-Control-Allow-Headers': 'Origin, Content-Type, X-Auth-Token'
    },
    withCredentials: true
    })
  }
}

export default new LoginService();
