import httpService from "./httpServices";
import { ACCOUNT } from "constants/api";

class UserService {
  getUser() {
    return httpService.get(ACCOUNT);
  }
}

export default new UserService();
