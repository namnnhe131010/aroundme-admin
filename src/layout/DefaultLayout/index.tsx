import Header from "components/Header";
import React, { Fragment, useState } from "react";
import "./styles.scss";
import SideBar from "../SideBar";

const DefaultLayout: React.FC = (props) => {
  const { children } = props;

  return (
    <>
      <div className="bg-home">
        <Header />
        <main>
          <div className="wrapper">
            <SideBar />
            <div id="content" style={{ paddingTop: 55 , width:'97%' }}>
              {children}
            </div>
          </div>
        </main>
      </div>
    </>
  );
};

export default DefaultLayout;
