import { Menu } from "constants/FixedMenu";
import React, { useState } from "react";
import { Link } from "react-router-dom";

const Index = () => {
  const [toggle, setToggle] = useState<boolean>(false);
  const [showSub, setShowSub] = useState<number>(-1);

  React.useEffect(() => {
    console.log(toggle, 'toggle');
    
  }, [toggle])

  return (
    <nav id="sidebar" className={toggle ? "active" : ""}>
      <div className="sidebar-header">
        {!toggle && (
          <a href="#">
            <div className="home"></div>
          </a>
        )}
        <div
          className="menu"
          onClick={() => {
            setToggle(!toggle);
            // setShowSub(-1);
          }}
        ></div>
      </div>

      <ul className="list-unstyled components">
        {Menu.map((item, index) => {
          return (
            <li className={`${showSub === index && "active"}`}>
              <Link
                // href={item.url}
                to={`${item.url}`}
                // data-toggle={`${toggle ? "" : "collapse"}`}
                // aria-expanded={showSub === index}
                // className={`dropdown-toggle ${showSub === index ? "collapsed" : ""}`}
                onClick={(e) => {
                  // e.preventDefault();

                  // if (index === showSub) {
                  //   setShowSub(-1);
                  //   return;
                  // }

                  setToggle(false);
                  // setShowSub(index);
                  //   setToggle(true);
                }}
              >
                <div style={{ display: "flex" }}>
                  {
                    !toggle &&<>
                     <div className={item.img}></div> <span className="pt-1 pl-1">{item.title}</span></>
                  }
                  
                </div>
              </Link>

              {/* <ul
                className={`collapse list-unstyled ${showSub === index && "show dropmenu-animation"}`}
                id="homeSubmenu"
              >
                {item.children.map((el) => (
                  <li>
                    <Link to={`${el.url}`}>{el.title}</Link>
                  </li>
                ))}
              </ul> */}
            </li>
          );
        })}
      </ul>
    </nav>
  );
};

export default Index;
