import React, { useState, useEffect } from "react";
import AppLoading from "../../components/AppLoading";
import AppMainContainer from "../../components/AppMainContainer";
import { convertToQuery } from "../../helpers/helperFunction";
import { put, sendGet } from "../../services/httpService";
import SearchForm from "./components/SearchForm";
import Tables from "./components/Tables";

const initValue = {
  name: "",
  gender: "",
  phone: "",
  cityId: -1,
  districtId: -1,
  wardsId: -1,
  status: -1,
  currentPage: 1,
};

export default function Index() {
  const [loading, setLoading] = useState(false);
  const [data, setData] = useState<any>([]);
  const [body, setBody] = useState(initValue);

  useEffect(() => {
    search();
  }, [body]);

  const search = () => {
    setLoading(true);
    sendGet(`api/searchUserByAdmin${convertToQuery(body)}`)
      .then((res: any) => {
        // console.log(res.data, "res");
        if (res?.status === 200 && res?.data) {
          setData(res?.data);
        }
      })
      .catch((err) => console.log("err", err))
      .finally(() => setLoading(false));
  };

  const update = async (user: any) => {
    try {
      const res = await put(`api/updateStatusUser`, user);
      if (res?.data?.status === 200) {
        setBody({ ...body });
      }
    } catch (error) {
      console.log("err", error);
    }
  };

  return (
    <AppMainContainer title="User">
      <AppLoading isvisible={loading} />
      <SearchForm
        setBody={(value: any) => {
          setBody({ ...value, currentPage: 1 });
        }}
        body={body}
      />
      <Tables
        dataTable={data}
        setBody={(page: any) => {
          setBody({ ...body, currentPage: page });
        }}
        update={update}
      />
    </AppMainContainer>
  );
}
