import AppButton from "components/AppButton";
import AppInput from "components/AppInput";
import AppSearchFormContainer from "components/AppSearchFormContainer";
import SelectField from "components/CustomField/SelectField";
import { Field, Form, Formik } from "formik";
import React, { useEffect } from "react";
import { Col, Row } from "reactstrap";
import SelectT from "components/AppSelectTQH/SelectT";
import SelectQ from "components/AppSelectTQH/SelectQ";
import SelectH from "components/AppSelectTQH/SelectH";
import { isEmpty } from "lodash";

const hieuLuc = [
  {
    label: "Deactive",
    value: '0',
  },
  {
    label: "Active",
    value: '1',
  },
];

const gender = [
  {
    label: "male",
    value: "male",
  },
  {
    label: "female",
    value: "female",
  },
];

export default (props: any) => {
  const { setBody } = props;

  const check = (obj:any) => {
    for (const key in obj) {
      if(isEmpty(obj[key])){
        obj[key] = -1;
      }else{
        obj[key] = +obj[key];
      }
    }

    return {...obj};
  }

  return (
    <Formik
      validateOnBlur={false}
      validateOnChange={false}
      initialValues={{
        name: "",
        gender: "",
        phone: "",
        cityId: -1,
        districtId: -1,
        wardsId: -1,
        status: -1,
        currentPage: 1,
      }}
      onSubmit={(values: any) => {
        let {name, gender, phone,...newValues} = values;

        newValues = check(newValues);
        
        if(!values.gender) {
          values.gender = ''
        }

        setBody({...values, ...newValues});
      }}
    >
      {(propsFormik) => {
        return (
          <Form>
            <AppSearchFormContainer title="Searching">
              <Row style={{ flex: 1 }}>
                <Col md="3">
                  <Field component={AppInput} name="name" title="Name" />
                </Col>
                <Col md="3">
                  <Field component={AppInput} name="phone" title="Phone Number" />
                </Col>
                <Col md="3">
                  <Field component={SelectField} name="status" title="Status" options={hieuLuc} />
                </Col>
                <Col md="3">
                  <Field component={SelectField} name="gender" title="Gender" options={gender} />
                </Col>
                <Col md="3">
                <Field component={SelectT} name="cityId" title="City" />
                </Col>
                <Col md="3">
                <Field
                    component={SelectQ}
                    name="districtId"
                    onReset={() => {
                      propsFormik.setFieldValue("districtId", "");
                    }}
                    cityId={propsFormik.values.cityId}
                    title="District"
                  />
                </Col>
                <Col md="3">
                  <Field
                    component={SelectH}
                    name="wardsId"
                    onReset={() => {
                      propsFormik.setFieldValue("wardsId", "");
                    }}
                    cityId={propsFormik.values.cityId}
                    districtId={propsFormik.values.districtId}
                    title="Ward"
                  />
                </Col>

                <Col md="3" className="d-flex justify-content-end pt-2">
                    <AppButton type="submit" label="Search" icon={<i className="fas fa-search"></i>} />
                </Col>
              </Row>
            </AppSearchFormContainer>
          </Form>
        );
      }}
    </Formik>
  );
};

// export default SearchForm;
