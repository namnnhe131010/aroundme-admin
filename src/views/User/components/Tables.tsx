import { DiaChinh } from "constants/jsonDiaChinh";
import { isEmpty } from "lodash";
import React, { useEffect, useState } from "react";
import "react-image-gallery/styles/css/image-gallery.css";
import Lightbox from "react-image-lightbox";
import "react-image-lightbox/style.css";
import { Col, Row } from "reactstrap";
import AppPage from "../../../components/AppPage";

interface Props {
  dataTable: any;
  setBody: any;
  update: any;
}

const URL = "http://172.104.189.141:86";

function TblDanhSachNgonNgu(props: Props) {
  const { dataTable, setBody, update } = props;
  const formikRef = React.useRef<any>();
  const [selectItem, setSelectItem] = useState<any>({});
  const [disabled, setDisabled] = useState(true);
  const [type, setType] = useState<String>("");
  const [image, setImage] = useState<any>("");
  const [isOpen, setIsOpen] = useState<any>(false);

  useEffect(() => {
    !isEmpty(selectItem) && formikRef.current.setValues(selectItem);
  }, [selectItem]);

  useEffect(() => {
    if (!isEmpty(image)) {
      setIsOpen(true);
    }
  }, [image]);

  function getStartIndexTableBySize(page: any, size: any) {
    return page * size - size;
  }

  const headerTable = () => {
    return (
      <thead>
        <tr>
          <th>STT</th>
          <th>Name</th>
          <th>About</th>
          <th>Date of Birth</th>
          <th>Phone Number</th>
          <th>Avatar</th>
          <th>Ward</th>
          <th>District</th>
          <th>City</th>
          <th>Detail Address</th>
          <th>Status</th>
          <th></th>
        </tr>
      </thead>
    );
  };

  const renderBody = () => {
    if (isEmpty(dataTable?.userList)) return null;

    let startIndex = getStartIndexTableBySize(dataTable?.currentPage, 10);
    return (
      <tbody>
        {!isEmpty(dataTable?.userList) &&
          dataTable?.userList?.map((el: any, idx: number) => {
            if (el?.roleId === 1) return null;
            return (
              <tr className={el?.id === selectItem?.id ? `hover-item` : ""} key={el?.id}>
                <td>{++startIndex}</td>
                <td>{el?.fullName}</td>
                <td>{el?.about}</td>
                <td>{el?.dob}</td>
                <td>{el?.phone}</td>
                <td>
                  {
                    <a
                      style={{ color: "#009e0f" }}
                      href="#"
                      onClick={(e) => {
                        e.preventDefault();
                        setImage(URL + el?.avatar);
                      }}
                    >
                      View
                    </a>
                  }
                </td>
                <td>{renderLocation(el?.cityId, el?.districtId, el?.wardsId, "W")}</td>
                <td>{renderLocation(el?.cityId, el?.districtId, 0, "D")}</td>
                <td>{renderLocation(el?.cityId, 0, 0, "C")}</td>
                <td>{el?.addressDetail}</td>
                <td>{renderStatus(el?.status)}</td>
                <td>{renderBtn(el)}</td>
              </tr>
            );
          })}
      </tbody>
    );
  };

  const renderStatus = (status: any) => {
    if (status === 1) {
      return <span className="badge badge-success p-2">Active</span>;
    }

    return (
      <span className="badge badge-danger p-2" style={{ backgroundColor: "#4d4e4e", color: "#fff" }}>
        Inactive
      </span>
    );
  };

  const renderBtn = (item: any) => {
    if (item?.status === 1) {
      return (
        <button
          onClick={() => {
            update({ ...item, status: 0 });
          }}
          className="btn"
          style={{ backgroundColor: "#4d4e4e", color: "#fff", fontSize: "75%" }}
        >
          Deactivate
        </button>
      );
    }

    return (
      <button
        onClick={() => {
          update({ ...item, status: 1 });
        }}
        className="btn btn-success"
        style={{ fontSize: "75%", marginRight: 5 }}
      >
        Activate
      </button>
    );
  };

  const renderLocation = (city: any, district: any, ward: any, type: any) => {
    let name;
    let selectedOption: any = DiaChinh.find((option: any) => option.value === city + "");

    if (type === "C") {
      name = selectedOption?.label;
    }

    selectedOption = [...selectedOption.lstCon].find((option) => option.value === district + "");
    if (type === "D" && !isEmpty(selectedOption?.lstCon)) {
      name = selectedOption?.label;
    }

    if (type === "W" && !isEmpty(selectedOption?.lstCon)) {
      selectedOption = [...selectedOption.lstCon].find((option) => option.value === ward + "");
      name = selectedOption?.label;
    }

    return name;
  };

  return (
    <Row>
      <Col md="12">
        <div className="content-table">
          <h4>Users</h4>
          <div className="height460">
            <table className="table fancyTable" id="myTable01" cellPadding={0} cellSpacing={0}>
              {headerTable()}
              {renderBody()}
            </table>
          </div>
          <AppPage
            totalPages={dataTable?.totalPage || "0"}
            currentPage={dataTable?.currentPage || "0"}
            currentSize={10}
            onChangePage={(page) => {
              setBody(page);
            }}
            // titleTotal="Số lượng"
          />
          {isOpen && image && (
            <Lightbox
              mainSrc={image}
              onCloseRequest={() => {
                setIsOpen(false);
                setImage("");
              }}
              onMovePrevRequest={() => {}}
              onMoveNextRequest={() => {}}
            />
          )}
        </div>
      </Col>

      {/* {renderButton()} */}
    </Row>
  );
}

export default TblDanhSachNgonNgu;
