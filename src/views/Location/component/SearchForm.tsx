import AppButton from "components/AppButton";
import AppInput from "components/AppInput";
import AppSearchFormContainer from "components/AppSearchFormContainer";
import SelectField from "components/CustomField/SelectField";
import { Field, Form, Formik } from "formik";
import React, { useEffect } from "react";
import { Col, Row } from "reactstrap";
import SelectT from "components/AppSelectTQH/SelectT";
import SelectQ from "components/AppSelectTQH/SelectQ";
import SelectH from "components/AppSelectTQH/SelectH";
import isEmpty from 'lodash/isEmpty'

const hieuLuc = [
  {
    label: "Pending",
    value: '0',
  },
  {
    label: "Approved",
    value: '1',
  },
  {
    label: "Reject",
    value: '2',
  },
];

export default (props: any) => {
  const { setBody } = props;

  const check = (obj:any) => {
    for (const key in obj) {
      if(isEmpty(obj[key])){
        obj[key] = -1;
      }else{
        obj[key] = +obj[key];
      }
    }

    return {...obj};
  }

  return (
    <Formik
      validateOnBlur={false}
      validateOnChange={false}
      initialValues={{
        name: "",
        categoryId: -1,
        cityId: -1,
        districtId: -1,
        wardsId: -1,
        phoneNumber: "",
        status: '0',
        currentPage: 1,
        addressDetail: ''
      }}
      onSubmit={(values: any) => {
        let {name, phoneNumber, addressDetail,...newValues} = values;
        newValues = check(newValues);
        setBody({...values, ...newValues});
      }}
    >
      {(propsFormik) => {
        return (
          <Form>
            <AppSearchFormContainer title="Searching">
              <Row style={{ flex: 1 }}>
                <Col md="4">
                  <Field component={AppInput} name="name" title="Name" />
                </Col>
                <Col md="4">
                  <Field component={AppInput} name="phoneNumber" title="Phone Number" />
                </Col>
                <Col md="4">
                  <Field component={SelectField} name="status" title="Status" options={hieuLuc} />
                </Col>
                <Col md="4">
                  <Field component={SelectT} name="cityId" title="City" />
                </Col>
                <Col md="4">
                  <Field
                    component={SelectQ}
                    name="districtId"
                    onReset={() => {
                      propsFormik.setFieldValue("districtId", "");
                    }}
                    cityId={propsFormik.values.cityId}
                    title="District"
                  />
                </Col>
                <Col md="4">
                  <Field
                    component={SelectH}
                    name="wardsId"
                    onReset={() => {
                      propsFormik.setFieldValue("wardsId", "");
                    }}
                    cityId={propsFormik.values.cityId}
                    districtId={propsFormik.values.districtId}
                    title="Ward"
                  />
                </Col>
                <Col md="12" className="d-flex justify-content-end">
                  <div style={{ marginLeft: "10px" }}>
                    <AppButton type="submit" label="Search" icon={<i className="fas fa-search"></i>} />
                  </div>
                </Col>
              </Row>
            </AppSearchFormContainer>
          </Form>
        );
      }}
    </Formik>
  );
};
