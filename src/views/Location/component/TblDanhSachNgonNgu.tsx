import { isEmpty } from "lodash";
import React, { useEffect, useRef, useState } from "react";
import "react-image-gallery/styles/css/image-gallery.css";
import Lightbox from "react-image-lightbox";
import "react-image-lightbox/style.css";
import { Col, Row } from "reactstrap";
import AppPage from "../../../components/AppPage";
import { DiaChinh } from "../../../constants/jsonDiaChinh";

interface Props {
  dataTable: any;
  setBody: any;
  update: any;
}

const URL = "http://172.104.189.141:86";

function TblDanhSachNgonNgu(props: Props) {
  const { dataTable, setBody, update } = props;
  const formikRef = React.useRef<any>();
  const [selectItem, setSelectItem] = useState<any>({});
  const [isFullScren, setFullScreen] = useState(false);
  const [images, setImages] = useState<any>([]);
  const [photoIndex, setPhotoIndex] = useState<any>(0);

  useEffect(() => {
    !isEmpty(selectItem) && formikRef.current.setValues(selectItem);
  }, [selectItem]);

  useEffect(() => {
    if (!isEmpty(images)) {
      // console.log(images, "images");
      setFullScreen(true);
    }
  }, [images]);

  useEffect(() => {
    console.log(photoIndex, "photoIndex");
  }, [photoIndex]);

  const headerTable = () => {
    return (
      <thead>
        <tr>
          <th>STT</th>
          <th>Name</th>
          <th>Description</th>
          <th>Ward</th>
          <th>District</th>
          <th>City</th>
          <th>Detail Address</th>
          <th>Image Verification</th>
          <th>Related Image</th>
          <th>Status</th>
          <th></th>
        </tr>
      </thead>
    );
  };

  const render = (status: any) => {
    if (status === 0) {
      return <span className="badge badge-warning p-2">Pending</span>;
    }

    if (status === 1) {
      return <span className="badge badge-success p-2">Approved</span>;
    }

    return (
      <span className="badge badge-danger p-2" style={{ backgroundColor: "#4d4e4e", color: "#fff" }}>
        Reject
      </span>
    );
  };

  const renderLocation = (city: any, district: any, ward: any, type: any) => {
    let name;
    let selectedOption: any = DiaChinh.find((option: any) => option.value === city + "");

    if (type === "C") {
      name = selectedOption?.label;
    }

    selectedOption = [...selectedOption.lstCon].find((option) => option.value === district + "");
    if (type === "D" && !isEmpty(selectedOption?.lstCon)) {
      name = selectedOption?.label;
    }

    if (type === "W" && !isEmpty(selectedOption?.lstCon)) {
      selectedOption = [...selectedOption.lstCon].find((option) => option.value === ward + "");
      name = selectedOption?.label;
    }

    return name;
  };

  const renderEdit = (item: any) => {
    if (item.status === 0) {
      return (
        <div>
          <button
            onClick={() => {
              update({ ...item, status: 1 });
            }}
            className="btn btn-success"
            style={{ fontSize: "75%", marginRight: 5 }}
          >
            Approve
          </button>
          <button
            onClick={() => {
              update({ ...item, status: 2 });
            }}
            className="btn"
            style={{ backgroundColor: "#4d4e4e", color: "#fff", fontSize: "75%" }}
          >
            Reject
          </button>
        </div>
      );
    }

    if (item.status === 1) {
      return (
        <div>
          <button
            onClick={() => {
              update({ ...item, status: 2 });
            }}
            className="btn"
            style={{ backgroundColor: "#4d4e4e", color: "#fff", fontSize: "75%" }}
          >
            Reject
          </button>
        </div>
      );
    }

    return null;
  };

  const convertImage = (img: any) => {
    let images = img.split("|");
    if (images.length >= 2) {
      const newList = [...images].map((item) => URL + item);
      setImages(newList);
    }
  };

  function getStartIndexTableBySize(page: any, size: any) {
    return page * size - size;
  }

  const renderBody = () => {
    if (isEmpty(dataTable?.locationList)) return null;

    let startIndex = getStartIndexTableBySize(dataTable?.currentPage, 10);

    return (
      <tbody>
        {!isEmpty(dataTable?.locationList) &&
          dataTable?.locationList?.map((el: any, idx: number) => {
            console.log(el, "el");

            return (
              <tr
                className={el?.id === selectItem?.id ? `hover-item` : ""}
                key={el?.id}
                // onClick={() => {
                //   disabled && setSelectItem(el);
                // }}
              >
                <td>{++startIndex}</td>
                <td>{el?.name}</td>
                <td>{el?.about}</td>
                <td>{renderLocation(el?.cityId, el?.districtId, el?.wardsId, "W")}</td>
                <td>{renderLocation(el?.cityId, el?.districtId, 0, "D")}</td>
                <td>{renderLocation(el?.cityId, 0, 0, "C")}</td>
                <td>
                  {
                    <a
                      style={{ color: "#009e0f" }}
                      href={`https://www.google.com/maps/search/?api=1&query=${el.latitude},${el.longitude}`}
                      target="_blank"
                    >
                      {el.addressDetail}
                    </a>
                  }
                </td>
                <td>
                  {
                    <a
                      style={{ color: "#009e0f" }}
                      href="#"
                      onClick={(e) => {
                        e.preventDefault();
                        convertImage(el.imageAuthen);
                      }}
                    >
                      View
                    </a>
                  }
                </td>
                <td>
                  {
                    <a
                      style={{ color: "#009e0f" }}
                      href="#"
                      onClick={(e) => {
                        e.preventDefault();
                        convertImage(el.image);
                      }}
                    >
                      View
                    </a>
                  }
                </td>

                <td>{render(el?.status)}</td>
                <td>{renderEdit(el)}</td>
              </tr>
            );
          })}
      </tbody>
    );
  };

  return (
    <Row>
      <Col md="12">
        <div className="content-table">
          <h4>Locations</h4>
          <div className="height460">
            <table className="table fancyTable" id="myTable01" cellPadding={0} cellSpacing={0}>
              {headerTable()}
              {renderBody()}
            </table>
            {/* <div style={{ display: isFullScren ? "block" : "none" }}> */}
            {/* <ImageGallery
                lazyLoad={true}
                ref={galleryRef}
                items={images}
                showThumbnails
                infinite={false}
                showBullets={true}
                showPlayButton={false}
                onScreenChange={(isFullScren) => {
                  setFullScreen(isFullScren);
                  if (!isFullScren) {
                    setImages([]);
                  }
                }}
              /> */}
            {isFullScren && (
              <Lightbox
                mainSrc={images[photoIndex]}
                nextSrc={images[(photoIndex + 1) % images.length]}
                prevSrc={images[(photoIndex + images.length - 1) % images.length]}
                onCloseRequest={() => {
                  console.log("close");
                  setFullScreen(false);
                  setImages([]);
                }}
                onMovePrevRequest={() => {
                  if (!images.length) return;
                  setPhotoIndex((photoIndex + images.length - 1) % images.length);
                }}
                onMoveNextRequest={() => {
                  if (!images.length) return;
                  setPhotoIndex((photoIndex + images.length - 1) % images.length);
                }}
              />
            )}

            {/* </div> */}
          </div>
          <AppPage
            totalPages={dataTable?.totalPage || "0"}
            currentPage={dataTable?.currentPage || "0"}
            currentSize={10}
            onChangePage={(page) => {
              setBody(page);
            }}
            titleTotal="Số lượng"
          />
        </div>
      </Col>

      {/* {renderButton()} */}
    </Row>
  );
}

export default TblDanhSachNgonNgu;
