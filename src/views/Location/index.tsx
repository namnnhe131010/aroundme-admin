import AppLoading from "components/AppLoading";
import AppMainContainer from "components/AppMainContainer";
import React, { useEffect, useState } from "react";
import { sendGet, put } from "services/httpService";
import { convertToQuery } from "../../helpers/helperFunction";
import SearchForm from "./component/SearchForm";
import TblDanhSachNgonNgu from "./component/TblDanhSachNgonNgu";

const initBody = {
  name: "",
  categoryId: -1,
  cityId: -1,
  districtId: -1,
  wardsId: -1,
  phoneNumber: "",
  status: '0',
  currentPage: 1,
};

const DanhMucNgonNgu = (props: any) => {
  const [loading, setLoading] = useState(false);
  const [dataTable, setDataTable] = useState<any>();
  const [body, setBody] = useState<any>(initBody);

  const search = () => {
    setLoading(true);
    sendGet(`api/searchLocationByAdmin${convertToQuery(body)}`)
      .then((res: any) => {
        if (res?.status === 200 && res?.data) {
          setDataTable(res.data);
        }
      })
      .catch((err) => console.log("err", err))
      .finally(() => setLoading(false));
  };

  const update = async (location: any) => {
    try {
      const res = await put(`api/location/updateStatusLocation`, location);
      if (res?.data?.status === 200) {
        setBody({ ...body });
      }
    } catch (error) {
      console.log("err", error);
    }
  };

  useEffect(() => {
    search();
  }, [body]);

  return (
    <AppMainContainer title="Location">
      <AppLoading isvisible={loading} />
      <SearchForm
        setBody={(value: any) => {
          setBody({ ...value, currentPage: 1 });
        }}
        body={body}
      />
      <TblDanhSachNgonNgu
        dataTable={dataTable}
        setBody={(page: any) => {
          setBody({ ...body, currentPage: page });
        }}
        update={update}
      />
    </AppMainContainer>
  );
};
export default DanhMucNgonNgu;
