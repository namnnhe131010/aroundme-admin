import AppLoading from "components/AppLoading";
import ErrorFocus from "components/ErrorFocus";
import { validateLoginSchema } from "constants/validation";
import { FastField, Field, Form, Formik } from "formik";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { useHistory } from "react-router-dom";
import { loginRequest, resetType } from "redux/modules/auth";
import { GetAuthSelector, GetTypeAuth } from "redux/selectors";
import * as types from "redux/types";
import InputField from "./CustomField/InputField";
import user2 from "../../assets/images/logoCA.svg";
import pw from "../../assets/images/text_home.svg";

const Login = (props: any) => {
  const [loading, setloading] = useState(false);
  const dispatch = useDispatch();
  const auth = GetAuthSelector();
  const authType = GetTypeAuth();
  const user: any = localStorage.getItem("user") || "";
  const { isLogin, error } = auth;
  let history = useHistory();

  const urlPath = window.location.hash;
  // const callbackUrl = urlPath.split("?callbackUrl=")?.[1] || "";

  // useEffect(() => {
  //   dispatch(resetType());
  // }, []);

  useEffect(() => {
    if (authType === types.REQUEST_LOGIN_SUCCESS) {
      history.push("/");
      // if (callbackUrl) {
      //   history.push(callbackUrl);
      //   return;
      // } else {
      //   return;
      // }
      // return;
    }
    // if (user) {
    //   history.push("/");
    // }
  }, [auth, authType]);

  return (
    <Formik
      validateOnChange={false}
      validateOnBlur={false}
      validationSchema={validateLoginSchema}
      initialValues={{
        username: "",
        password: "",
        rememberMe: false,
      }}
      onSubmit={(values) => {
        const { ...remainValues } = values;
        setloading(true);
        dispatch(loginRequest(remainValues, setloading));
      }}
    >
      {(propsFormik) => (
        <div className="d-flex justify-content-center align-items-center" style={{ width: "100%", height: "100vh" }}>
          <div className="login w-100">
            <div className="form-search">
              <div className="login-content">
                <Form className="mb-0">
                  <ErrorFocus />
                  <div className="input-group">
                    <span className="input-group-addon">
                      <img src="/assets/images/user.svg" width="20px" height="20px" alt="" />
                    </span>

                    <FastField
                      component={InputField}
                      placeholder="username"
                      name="username"
                      style={{ width: "100%", paddingTop: 0 }}
                    />
                  </div>
                  <div className="input-group">
                    <span className="input-group-addon">
                      <img src="/assets/images/pass.svg" width="18px" height="20px" alt="" />
                    </span>
                    <FastField
                      component={InputField}
                      placeholder="Password"
                      name="password"
                      type="password"
                      style={{ width: "100%", paddingTop: 0 }}
                    />
                 
                  </div>

                  {error && <p style={{ color: "red" }}>Wrong username or password</p>}
                  <div className="remember-login col-12 p-0 pt-4">
                    {/* <div className="rememberPassword col-12 p-0 pt-2 pb-5">
                      <div className="checkbox">
                        <label className="containCheck">
                          <Field component={InputField} name="rememberMe" type="checkbox" />
                          <span className="title-ck ml-2">Nhớ mật khẩu</span>
                          <span className="checkmark"></span>
                        </label>
                      </div>
                    </div> */}
                    <div className="col-12 p-0">
                      <button type="submit" className="btn btn-primary btn-dangnhap ">
                        Login
                      </button>
                    </div>
                  </div>
                </Form>
              </div>
            </div>
          </div>
        </div>
      )}
    </Formik>
  );
};
export default Login;
