import React from "react";
import { HashRouter as Router, Route, Switch } from "react-router-dom";

import "./scss/styles.scss";
import SecureRoute from "routes/SecureRoute";
import PrivateRoute from "components/PrivateRoute";
import { RouteBase } from "constants/routeUrl";
import AppToastContainer from "components/AppToastContainer";
import "react-toastify/dist/ReactToastify.css";
import Login from "views/Login";
import "@fortawesome/fontawesome-free/css/all.css";
const App: React.FC = () => {
  // RENDER

  return (
    <Router>
      <Switch>
        <Route path={RouteBase.Login} exact component={Login} />
        <PrivateRoute path="/" component={SecureRoute} />
      </Switch>
      <AppToastContainer />
    </Router>
  );
};

export default App;
