import moment from "moment";
import React, { useEffect, useState } from "react";
import DatePicker, { ReactDatePickerProps } from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import dateLogo from "assets/images/date.svg";
import MaskedTextInput from "react-text-mask";
import vi from "date-fns/locale/vi";

interface Props extends ReactDatePickerProps {
  title?: string;
  horizontal?: boolean;
  defaultDate?: any;
  isRequired?: boolean;
  noLabel?: any;
  field?: any;
  form?: any;
  disabled?: boolean;
  placeholder?: string;
  style?: any
}

const DATE_FORMAT = "DD_MM_YYYY HH:mm:ss";

export default React.memo((props:Props ) => {
  const {
    title,
    noLabel,
    isRequired,
    horizontal,
    defaultDate,
    field,
    form,
    disabled,
    className,
    placeholder,
    style,
    maxDate,
    minDate,
    ...remainProps
  } = props;

  const [open, setOpen] = useState(false);
  const [value, setValue] = useState("");

  const [date, setDate] = useState(new Date(14 - 12 - 1995));
  const handleChange = (date: any) => setDate(date);

  useEffect(() => {
    setValue(defaultDate);
  }, [defaultDate]);

  useEffect(() => {
    setValue(field?.value || "");
  }, [field?.value]);

  const onChangeCalendarSelected = (dateSelected: any):void => {
    if (field) {
      form.setFieldValue(
        field?.name,
        dateSelected ? moment(dateSelected).format("DD-MM-YYYY HH:mm:ss") : ""
      );
    }
    setValue(moment(dateSelected).format("DD-MM-YYYY HH:mm:ss"));
  };

  const processDateInput = (value: any) => {
    if (value && value.length === 19 && !value.includes("_")) {
      const dateInput = moment(value, DATE_FORMAT);
      if (dateInput.isValid()) {
        if (minDate && dateInput.isBefore(minDate)) {
          return moment(minDate).format("DDMMYYYYHHmmss");
        }
        if (maxDate && dateInput.isAfter(maxDate)) {
          return moment(maxDate).format("DDMMYYYYHHmmss");
        }
        return value;
      } else {
        return "";
      }
    }
    return value;
  };

  const onBlurCheckDate = (value:any) => {
    if (value.includes("_")) {
      return "";
    }
    return value;
  };

  return (
    <div className="date-picker-container" style={style}>
      <div className="app-date-picker-wrap">
        {title ? (
          <label>
            {title || ""} {isRequired && <span className="error">*</span>}
          </label>
        ) : (
          <div style={{ marginTop: "10px" }}> </div>
        )}
        <div className="datepicker-content">
          <MaskedTextInput
            {...field}
            name={field.name}
            placeholder={placeholder || ""}
            disabled={disabled}
            type="text"
            autoComplete="off"
            value={field.value}
            className={`${
              form.errors[field.name] &&
              form.touched[field.name] &&
              "is-invalid"
            }`}
            mask={[
              /\d/,
              /\d/,
              "-",
              /\d/,
              /\d/,
              "-",
              /\d/,
              /\d/,
              /\d/,
              /\d/,
              " ",
              /\d/,
              /\d/,
              ":",
              /\d/,
              /\d/,
              ":",
              /\d/,
              /\d/,
            ]}
            onChange={(e) => {
              form.setFieldValue(field.name, processDateInput(e.target.value));
              setValue(processDateInput(e.target.value));
              setOpen(false);
            }}
            onBlur={(e) => {
              form.setFieldValue(field.name, onBlurCheckDate(e.target.value));
              setValue(onBlurCheckDate(e.target.value));
            }}
            onClick={(e) => {
              setOpen(true);
            }}
            onKeyDown={(e) => {
              if (e.key == "Enter") {
                e.preventDefault();
                setOpen(false);
              }
            }}
          />

          <label>
            <span
              className="calender-icon"
              onClick={() => !disabled && setOpen(true)}
            >
              <img src={dateLogo} height="12px" width="12px" alt="" />
            </span>
          </label>
          {form?.touched[field.name] && form?.errors[field.name] && (
            <div className="err-text">{form.errors[field.name]}</div>
          )}
        </div>
      </div>

      <DatePicker
        showTimeInput
        timeInputLabel="Thời gian:"
        onClickOutside={() => setOpen(false)}
        open={open}
        selected={
          !moment(value, "DD-MM-YYYY HH:mm:ss").isValid()
            ? null
            : moment(value, "DD-MM-YYYY HH:mm:ss").toDate()
        }
        className="datePicker-input"
        disabled={disabled}
        {...remainProps}
        onChange={(dateSelected) => {onChangeCalendarSelected(dateSelected)}}
        popperPlacement="bottom-end"
        maxDate={maxDate}
        minDate={minDate}
        showMonthDropdown
        showYearDropdown
        dropdownMode="select"
        autoComplete="off"
        locale={vi}
      />
    </div>
  );
});
