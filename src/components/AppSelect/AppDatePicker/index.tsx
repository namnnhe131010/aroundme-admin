import moment from "moment";
import React, { useEffect, useState } from "react";
import DatePicker, { ReactDatePickerProps } from "react-datepicker";
import MaskedTextInput from "react-text-mask";

interface Props extends ReactDatePickerProps {
  title?: string;
  horizontal?: boolean;
  defaultDate?: any;
  isRequired?: any;
  noLabel?: any;
  field?: any;
  form?: any;
  disabled?: boolean;
}

const DATE_FORMAT = "DD_MM_YYYY";

const months = [
  "Tháng 01",
  "Tháng 02",
  "Tháng 03",
  "Tháng 04",
  "Tháng 05",
  "Tháng 06",
  "Tháng 07",
  "Tháng 08",
  "Tháng 09",
  "Tháng 10",
  "Tháng 11",
  "Tháng 12",
];
const days = ["CN", "T2", "T3", "T4", "T5", "T6", "T7"];

const locale = {
  localize: {
    month: (n: any) => months[n],
    day: (n: any) => days[n],
  },
  formatLong: {},
} as Locale;

export default React.memo((props: Props) => {
  const { title, noLabel, isRequired, horizontal, defaultDate, field, form, disabled, ...remainProps } = props;

  const [open, setOpen] = useState(false);
  const [value, setValue] = useState("");

  useEffect(() => {
    setValue(defaultDate);
  }, [defaultDate]);

  useEffect(() => {
    setValue(field?.value || "");
  }, [field?.value]);

  const processDateInput = (value: any) => {
    if (value && value.length === 10 && !value.includes("_")) {
      const dateInput = moment(value, DATE_FORMAT);
      if (dateInput.isValid()) {
        if (remainProps.minDate && dateInput.isBefore(remainProps.minDate)) {
          return moment(remainProps.minDate).format("DDMMYYYY");
        }
        if (remainProps.maxDate && dateInput.isAfter(remainProps.maxDate)) {
          return moment(remainProps.maxDate).format("DDMMYYYY");
        }
        return value;
      } else {
        return "";
      }
    }
    return value;
  };

  const onChangeCalendarSelected = (dateSelected: any) => {
    if (field) {
      // if (remainProps.minDate && moment(dateSelected).isBefore(remainProps.minDate)) {
      //   return form.setFieldValue(field.name, moment(remainProps.minDate).format("DD-MM-YYYY"));
      // }
      form.setFieldValue(field.name, moment(dateSelected).format("DD-MM-YYYY"));
    }
    setValue(moment(dateSelected).format("DD-MM-YYYY"));
  };

  const onBlurCheckDate = (value: any) => {
    if (value.includes("_")) {
      return "";
    }
    return value;
  };

  const renderPicker = () => {
    if (open) {
      if (field) {
        return (
          <DatePicker
            {...field}
            {...remainProps}
            onClickOutside={() => setOpen(false)}
            open={true}
            className="datePicker-input"
            selected={moment(value, "DD-MM-YYYY").isValid() ? moment(value, "DD-MM-YYYY").toDate() : moment().toDate()}
            filterDate={props.filterDate}
            customInput={<div style={{ width: "50px", height: 1 }} />}
            autoComplete="off"
            dateFormat="dd-MM-yyyy"
            onChange={onChangeCalendarSelected}
            locale={locale}
          />
        );
      }
      return (
        <DatePicker
          {...remainProps}
          onClickOutside={() => setOpen(false)}
          open={true}
          className="datePicker-input"
          selected={moment(value, "DD-MM-YYYY").isValid() ? moment(value, "DD-MM-YYYY").toDate() : moment().toDate()}
          filterDate={props.filterDate}
          customInput={<div className="border border-danger" />}
          autoComplete="off"
          dateFormat="dd-MM-yyyy"
          onChange={onChangeCalendarSelected}
          locale={locale}
          disabled={disabled}
        />
      );
    }
  };

  const renderInputPicker = () => {
    if (field) {
      return (
        <div className="app-date-picker-wrap">
          <label>
            {title || ""} {isRequired && <span className="required">*</span>}
          </label>
          <div className="datepicker-content">
            <MaskedTextInput
              name={field.name}
              disabled={disabled}
              type="text"
              value={field.value}
              mask={[/\d/, /\d/, "-", /\d/, /\d/, "-", /\d/, /\d/, /\d/, /\d/]}
              onChange={(e) => {
                form.setFieldValue(field.name, processDateInput(e.target.value));
                setValue(processDateInput(e.target.value));
              }}
              onBlur={(e: any) => {
                form.setFieldValue(field.name, onBlurCheckDate(e.target.value));
                setValue(onBlurCheckDate(e.target.value));
              }}
            />

            <label>
              <span className="calender-icon" onClick={() => !disabled && setOpen(true)}>
                <img src="/assets/images/date.svg" height="12px" width="12px" alt="" />
              </span>
            </label>
            {form?.touched[field.name] && form?.errors[field.name] && (
              <div className="err-text">{form.errors[field.name]}</div>
            )}
          </div>
        </div>
      );
    }

    return (
      <div className="app-date-picker-wrap">
        <label>
          {title || ""} {isRequired && <span className="required">*</span>}
        </label>
        <div className="datepicker-content">
          <MaskedTextInput
            type="text"
            value={value}
            mask={[/\d/, /\d/, "-", /\d/, /\d/, "-", /\d/, /\d/, /\d/, /\d/]}
            onChange={(e) => setValue(processDateInput(e.target.value))}
            onBlur={(e: any) => setValue(onBlurCheckDate(e.target.value))}
            disabled={disabled}
          />

          <label>
            <span className="calender-icon" onClick={() => !disabled && setOpen(true)}>
              <img src="/assets/images/date.svg" height="12px" width="12px" alt="" />
            </span>
          </label>
        </div>
      </div>
    );
  };

  return (
    <>
      <div className="date-picker-container">
        {renderInputPicker()}
        {renderPicker()}
      </div>
    </>
  );
});
