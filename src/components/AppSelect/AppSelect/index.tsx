import React from "react";
import Select, { components, OptionTypeBase, Props as SelectProps } from "react-select";

interface Props extends SelectProps<OptionTypeBase> {
  title?: string;
  horizontal?: boolean;
  options?: Array<{
    label: string;
    value: any;
  }>;
  dropDownIcon?: boolean;
  isRequired?: boolean;
  error?: any;
  isClearable?: boolean;
  refs?: any;
}
const DropdownIndicator = (props: any) => {
  return (
    <components.DropdownIndicator {...props}>
      <img src="/assets/images/arrowdown.svg" width="7px" alt="..." />
    </components.DropdownIndicator>
  );
};

export default React.memo((props: Props) => {
  const { title, options, horizontal, isRequired, dropDownIcon, error, isClearable, refs, ...remainProps } = props;

  const customStyles = {
    // Nếu có ...provider thì sẽ mặc định style cũ
    control: (provided: any, state: any) => ({
      ...provided,
      borderColor: "#9e9e9e",
      minHeight: "25px",
      height: "25px",
      boxShadow: state.isFocused ? `0 0 0 0.2rem rgba(0, 123, 255, 0.25)` : null,
      // border: "none",
      background: "#f3f3f3",
      borderRadius: 4,
      border: "1px solid #bfbfbf",
    }),

    valueContainer: (provided: any, state: any) => {
      return {
        ...provided,
        height: "23px",
        padding: "0 6px",
        background: state.isDisabled ? "none" : "#ffffff",
        // borderBottom: "1px solid #bfbfbf",
        // border: state.isDisabled ? 0 : "1px solid #bfbfbf",
        // borderRightWidth: 0,
        // borderRadius: 4,
        borderTopLeftRadius: 4,
        borderBottomLeftRadius: 4,
      };
    },
    menu: (provided: any, state: any) => ({
      ...provided,
      zIndex: 100,
    }),
    input: (provided: any, state: any) => ({
      ...provided,
      margin: "0px",
      width: "auto",
    }),
    indicatorSeparator: (provided: any, state: any) => ({
      // Thanh phân cách giữa input và nút xuống
      display: "none",
    }),
    indicatorsContainer: (provided: any, state: any) => ({
      ...provided,
      height: "23px",
      background: state.isDisabled ? "none" : "#ffffff",
      // border: state.isDisabled ? 0 : "1px solid #bfbfbf",
      // borderLeftWidth: 0,
      borderTopRightRadius: 4,
      borderBottomRightRadius: 4,
    }),
    dropdownIndicator: (state: any) => ({
      // Mũi tên trỏ xuống
      // display: dropDownIcon ? "none" : "block",
      margin: "6px",
      // background: "#ffffff",
      background: props.isDisabled ? "#f3f3f3" : "#ffffff",
    }),
    loadingIndicator: (provided: any, state: any) => ({
      // loading style
      ...provided,
    }),
    singleValue: (provided: any, state: any) => ({
      ...provided,
      color: "#000",
    }),
  };
  return (
    <div className={`app-select-wrap ${horizontal ? "horizontal" : ""}`}>
      {title && (
        <label>
          {title} {isRequired && <span className="required">*</span>}
        </label>
      )}
      <div className="cccd-custom-select">
        <Select
          {...remainProps}
          ref={refs && refs}
          styles={customStyles}
          isClearable={isClearable === false ? isClearable : true}
          placeholder=""
          options={options || []}
          noOptionsMessage={() => "Không có dữ liệu"}
          loadingMessage={() => "Đang tải dữ liệu"}
          components={{ DropdownIndicator }}
        />
        {error && <div className="err-text">{error}</div>}
      </div>
    </div>
  );
});
