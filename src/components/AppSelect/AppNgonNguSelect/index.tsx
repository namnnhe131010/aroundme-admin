import AppSelect from "components/AppSelect/AppSelect";
import React, { Fragment, useEffect, useState } from "react";
import { BASE_URL, sendPost } from "services/httpService";
import httpServices from "services/httpServices";

const InputField = (props: any) => {
  const { form, field, placeholder, disabled, title, isRequired, getObj } = props;
  const [data, setData] = useState<any[]>([]);
  const [loading, setLoading] = useState(true);

  const getDataNgonNgu = () => {
    sendPost(`${BASE_URL}/bviet/api/ngon-ngu/tim-kiem`, { status: 1 })
      .then((res: any) => {
        if (res.data.code === 200) {
          setData(
            res.data.data.map((el: any) => {
              return { label: el.name, value: el.id };
            })
          );
        }
      })
      .catch((err) => console.log("err", err))
      .finally(() => {
        setLoading(false);
      });
  };
  useEffect(() => {
    getDataNgonNgu();
  }, []);

  const { name, value } = field;
  const { errors, touched } = form;

  const selectedOption = data.find((option: any) => option.value === value);

  const handleSelectedOptionChange = (selectedOption: any) => {
    const selectedValue = selectedOption ? selectedOption.value : selectedOption;

    props.getObj && props.getObj(selectedOption);

    const changeEvent = {
      target: {
        name: name,
        value: selectedValue,
      },
    };

    field.onChange(changeEvent);
  };

  return (
    <Fragment>
      <AppSelect
        {...field}
        isLoading={loading}
        title={title}
        value={selectedOption || ""}
        onChange={handleSelectedOptionChange}
        placeholder={placeholder}
        isDisabled={disabled}
        isRequired={isRequired}
        options={data}
        getOptionLabel={(item) => `${item.label}`}
        getOptionValue={(item) => item.value}
      />
      {errors[name] && <div style={{ color: "red", fontSize: "10px", fontStyle: "italic" }}>{errors[name]}</div>}
    </Fragment>
  );
};

InputField.defaultProps = {
  type: "text",
  tabIndex: "0",
  invalid: "false",
};

export default InputField;
