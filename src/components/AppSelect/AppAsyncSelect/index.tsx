import React from "react";
import { components, OptionTypeBase } from "react-select";
import SelectAsync, { Props as AsyncProps } from "react-select/async";


interface Props extends AsyncProps<OptionTypeBase> {
  title?: string;
  horizontal?: boolean;
  options?: Array<{
    label: string;
    value: any;
  }>;
  dropDownIcon?: boolean;
  isRequired?: boolean;
  error?: any;
}
const DropdownIndicator = (props: any) => {
  return (
    <components.DropdownIndicator {...props}>
      <img src="/assets/images/arrowdown.svg" width="7px" alt="sdsd" />
    </components.DropdownIndicator>
  );
};

export default React.memo((props: Props) => {
  const { title, options, horizontal, isRequired, dropDownIcon, error, ...remainProps } = props;

  const customStyles = {
    // Nếu có ...provider thì sẽ mặc định style cũ
    control: (provided: any, state: any) => ({
      ...provided,
      borderColor: "#9e9e9e",
      minHeight: "25px",
      height: "25px",
      boxShadow: state.isFocused ? null : null,
      border: "none",
      background: "#e8e8e8",
      borderRadius: 0,
      borderBottom: "1px solid #bfbfbf",
    }),

    valueContainer: (provided: any, state: any) => {
      return {
        ...provided,
        height: "25px",
        padding: "0 6px",
        background: "#e8e8e8",
        // borderBottom: "1px solid #bfbfbf",
        borderBottom: state.isDisabled ? 0 : "1px solid #bfbfbf",
      };
    },
    menu: (provided: any, state: any) => ({
      ...provided,
      zIndex: 100,
    }),
    input: (provided: any, state: any) => ({
      ...provided,
      margin: "0px",
      width: "auto",
    }),
    indicatorSeparator: (provided: any, state: any) => ({
      // Thanh phân cách giữa input và nút xuống
      display: "none",
    }),
    indicatorsContainer: (provided: any, state: any) => ({
      ...provided,
      height: "25px",
      background: "#e8e8e8",
      borderBottom: state.isDisabled ? 0 : "1px solid #bfbfbf",
    }),
    dropdownIndicator: (state: any) => ({
      // Mũi tên trỏ xuống
      // display: dropDownIcon ? "none" : "block",
      margin: "6px",
      background: "#e8e8e8",
    }),
    loadingIndicator: (provided: any, state: any) => ({
      // loading style
      ...provided,
    }),
    singleValue: (provided: any, state: any) => ({
      color: "#000"
    })
  };

  return (
    <div className={`app-select-wrap ${horizontal ? "horizontal" : ""}`}>
      {title && (
        <label>
          {title} {isRequired && <span className="required">*</span>}
        </label>
      )}
      <div className="cccd-custom-select">
        <SelectAsync
          {...remainProps}
          styles={customStyles}
          isClearable
          placeholder=""
          cacheOptions
          options={options || []}
          noOptionsMessage={() => "Không có dữ liệu"}
          loadingMessage={() => "Đang tải dữ liệu"}
          components={{ DropdownIndicator }}
          // getOptionLabel={(item: any) => `${item.tenDiaChinh}`}
          // getOptionValue={(item: any) => item.id}
        />
        {error && <div className="err-text">{error}</div>}
      </div>
    </div>
  );
});
