import React from "react";

const styleTitle: React.CSSProperties = {
  fontFamily: "Myriad Bold !important",
};
const styleContent: React.CSSProperties = {
  fontFamily: "Myriad !important",
};

interface Props {
  title?: string;
  description?: string;
}

export default React.memo((props: Props) => {
  const { title, description } = props;
  return (
    <div>
      {title && <p style={styleTitle}>{title}</p>}
      {description && <p style={styleContent}>{description}</p>}
    </div>
  );
});
