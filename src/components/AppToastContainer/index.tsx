import React from "react";
import { toast, ToastContainer, ToastOptions } from "react-toastify";
import ToastContent from "./Components/ToastContent";
/**
 *
 * @example
 * appToast({ title: "string", description: "string", toastOptions: { type: "success" } });
 */
export const appToast = (props: { title?: string; description?: string; toastOptions?: ToastOptions }) => {
  const { title, toastOptions } = props;
  let description = props.description;
  if (props?.toastOptions?.type === "success") {
    description = description || "Success";
  }
  if (props?.toastOptions?.type === "error") {
    description = description || "Error";
  }
  toast(<ToastContent title={title} description={description} />, toastOptions ? toastOptions : {});
};

export default React.memo(() => {
  return (
    <ToastContainer
      position="top-right"
      autoClose={3000}
      hideProgressBar={true}
      newestOnTop
      closeOnClick
      rtl={false}
      pauseOnFocusLoss={false}
      draggable
      pauseOnHover
      style={{ marginTop: 55 }}
    />
  );
});
