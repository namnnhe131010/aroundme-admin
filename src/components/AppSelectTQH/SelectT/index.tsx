import AppSelect from "components/AppSelect/AppSelect";
import { convertToFormSelect } from "constants/selectUlts";
import React, { Fragment } from "react";
import { DiaChinh } from "constants/jsonDiaChinh";

const InputField = (props: any) => {
  const { form, field, placeholder, disabled, options, title, isRequired } = props;
  // const [data, setdata] = useState<any[]>([]);
  const { name, value } = field;
  const { errors, touched } = form;

  const selectedOption = DiaChinh.find((option: any) => option.value === value);

  const handleSelectedOptionChange = (selectedOption: any) => {
    // console.log(selectedOption, "selectedOption");
    const selectedValue = selectedOption ? selectedOption.value : selectedOption;

    const changeEvent = {
      target: {
        name: name,
        value: selectedValue,
      },
    };

    field.onChange(changeEvent);
  };

  return (
    <Fragment>
      <AppSelect
        {...field}
        title={title}
        value={selectedOption || ""}
        onChange={handleSelectedOptionChange}
        placeholder={placeholder}
        isDisabled={disabled}
        isRequired={isRequired}
        options={convertToFormSelect(DiaChinh, "label", "value")}
      />
      {errors[name] && <div style={{ color: "red", fontSize: "10px", fontStyle: "italic" }}>{errors[name]}</div>}
    </Fragment>
  );
};

InputField.defaultProps = {
  type: "text",
  tabIndex: "0",
  invalid: "false",
};

export default InputField;
