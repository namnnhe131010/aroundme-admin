import ErrorBoundary from "components/ErrorBoundary";
import { RouteBase } from "constants/routeUrl";
import { getLocalMenu } from "helpers/getLocalStore";
import React, { useEffect, useState } from "react";

const convertSpecialLink = (linkUrl: string) => {
  return [linkUrl, `${linkUrl}/`];
};

const specialLink = [
  "/",
  "/quan-ly-bai-viet/soan-bai-viet",
  '/location',
  '/category',
  '/user'
];

const NoPermission = (props: any) => {
  useEffect(() => {
    // dispatch(getUserRequest());
    document.body.classList.remove("bg-login");
    document.body.classList.add("bg-home-index");
    return () => {
      document.body.classList.remove("bg-home-index");
    };
  }, []);
  return <div style={{display: 'flex', alignItems: 'center', justifyContent: 'center', width: '100%', height: '100%'}}>Not Found</div>;
};

const withErrorBoundary = (BaseComponent: any, permissionAllowed: any = "") => {
  return (props: any) => {
    const [state, setState] = useState({ loading: true, havePermission: false });
    const urlPath = window.location.hash;

    useEffect(() => {
      // get permission menu
      const menu = getLocalMenu();
      const stringMenu = JSON.stringify(menu);
      // get url
      const splitUrl = urlPath.split("/");
      // create url after split
      const reCreateUrl = `/${splitUrl?.[1] || ""}`;
      let sliceUrl = reCreateUrl;
     
      if (urlPath.includes("?")) {
        // remove search params
        sliceUrl = reCreateUrl.split("?")?.[0];
      }

      // check permission view page
      if (stringMenu.includes(sliceUrl) || specialLink.includes(sliceUrl)) {
        // have permission view page
        setState({ loading: false, havePermission: true });
      } else {
        // no permission view page
        setState({ loading: false, havePermission: false });
      }
    }, [urlPath]);

    // while loading permission
    if (state.loading) {
      return null;
    }
    // while loaded permission
    if (state.havePermission) {
      // have permission view page
      return (
        <ErrorBoundary>
          <BaseComponent {...props} />
        </ErrorBoundary>
      );
    } else {
      // no permission view page
      return <NoPermission />;
    }
  };
};

export default withErrorBoundary;
