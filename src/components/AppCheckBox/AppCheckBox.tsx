import React from "react";
export default (props: any) => {
  const { title, field, disabled } = props;
  const { name, value } = field;
  return (
    <div className="content">
      <label className="containCheck">
        <input
          {...field}
          type="checkbox"
          id={name}
          checked={value}
          onChange={field.onChange}
          disabled={disabled || false}
        />
        <span className="checkmark" />
        <span className="title ml-2">{title && <label>{title}</label>}</span>
      </label>
    </div>
  );
};
