import React from "react";
interface Props extends React.InputHTMLAttributes<HTMLInputElement> {
  title?: string;
  horizontal?: boolean;
  noLabel?: boolean;
  isRequired?: boolean;
  wrapStyle?: any;
  field?: any;
  form?: any;
  marginLeft?: any;
  marked?: boolean;
  // type?: any;
}

export default React.forwardRef((props: Props, ref: any) => {
  const {
    marginLeft,
    title,
    noLabel,
    readOnly,
    horizontal,
    isRequired,
    wrapStyle,
    field,
    form,
    marked,
    ...remainProps
  } = props;

  return (
    <div className={`app-input-wrap ${horizontal ? "horizontal" : ""} ${marked ? "marked" : ""} `} style={wrapStyle}>
      {noLabel ? (
        ""
      ) : (
        <label>
          {title || ""} {isRequired && <span className="required">*</span>}
        </label>
      )}

      <input
        {...field}
        ref={ref}
        className={`form-control`}
        readOnly={readOnly}
        {...remainProps}
        style={marginLeft ? { marginLeft: 15 } : props.style}
      />
    </div>
  );
});
