import AppButton from "components/AppButton";
import iconSrc from "helpers/iconSrc";
import React from "react";
import { Modal } from "reactstrap";

export enum ModalType {
  question = "question",
  success = "success",
  warning = "warning",
  none = "",
}

interface Props {
  openModal: boolean;
  onPressYes?: () => void;
  onCloseModal?: () => void;
  className?: string;
  // modalHeaderTitle?: string;
  type?: ModalType;
  description?: string;
}

const styles = {
  btnWrap: {
    background: "#f3f3f3",
    borderBottomLeftRadius: 8,
    borderBottomRightRadius: 8,
  },
};

export default (props: Props) => {
  const { openModal, className, type, description, onPressYes, onCloseModal } = props;

  if (!Boolean(type)) {
    return null;
  }

  const createImageSource = () => {
    switch (type) {
      case "question":
        return iconSrc.iQuestion;
      case "warning":
        return iconSrc.iWarning;
      default:
        return iconSrc.iSuccess;
    }
  };

  const renderButton = () => {
    if (type === "question") {
      return (
        <>
          <AppButton img="/assets/icon/icon_success_white.svg" label="Đồng ý" onClick={onPressYes} />
          <AppButton img="/assets/images/close.svg" label="Hủy" onClick={onCloseModal} />
        </>
      );
    }
    return <AppButton img="/assets/images/close.svg" label="Đóng" onClick={onCloseModal} />;
  };

  return (
    <Modal isOpen={openModal} onClosed={onCloseModal} className={className}>
      <div className="align-content-center d-flex justify-content-center p-4">
        <img src={createImageSource()} height="72px" width="72px" alt="" />
      </div>
      <div className="text-center custom-desc-modal pb-4">{description}</div>
      <div className="in-block border-0" style={styles.btnWrap}>
        <div className="bg_in_btn">{renderButton()}</div>
      </div>
    </Modal>
  );
};
