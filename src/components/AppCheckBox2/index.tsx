import React from "react";

interface Props extends React.InputHTMLAttributes<HTMLInputElement> {
  title?: string;
  isRadio?: boolean;
  isFormik?: boolean;
  field?: any;
  valueChecked?: any;
  style?: any;
  styleWrapper?: any;
  form?: any;
}

export default React.memo((props: Props) => {
  const { title, isRadio, form, field, valueChecked, isFormik, style, styleWrapper, ...remainProps } = props;

  const renderType = () => {
    if (isFormik || field) {
      return (
        <div className={`checkbox-wrapper max-width-auto ${isRadio ? "radioType" : ""}`}>
          <label className="containCheck"  style={remainProps.disabled ? {cursor: 'default'} : {}}>
            <input
              {...field}
              value={valueChecked || field.value}
              checked={valueChecked && field.value === valueChecked}
              type={isRadio ? "radio" : "checkbox"}
              {...remainProps}
            />
            {/* {console.log('remainProps.disabled', remainProps.disabled)
            } */}
            <span className="checkmark" style={remainProps.disabled ? {pointerEvents: 'none'} : {}} />
            <span className="checkbox-label">{title}</span>
          </label>
        </div>
      );
    }
    return (
      <div
        className={`checkbox-wrapper max-width-auto ${isRadio ? "radioType" : ""} ${style ? style : ""}`}
        style={styleWrapper}
      >
        <label className="containCheck"  style={remainProps.disabled ? {cursor: 'default'} : {}}>
          <input type={isRadio ? "radio" : "checkbox"} value={valueChecked} {...remainProps} />
          <span className="checkmark" style={remainProps.disabled ? {cursor: 'default'} : {}} />
          <span className="checkbox-label">{title}</span>
        </label>
      </div>
    );
  };

  return renderType();
});
