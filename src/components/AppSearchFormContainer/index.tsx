import iconSrc from "helpers/iconSrc";
import React from "react";

interface Props {
  title?: any;
  children?: any;
  withImage?: any;
  records?: any;
  number?: any;
  isFirst?: () => void;
  isNext?: () => void;
  titleNone?: boolean;
}

export default (props: Props) => {
  const { title, withImage, children, records, number, isFirst, isNext, titleNone } = props;
  const renderRecords = () => {
    return (
      <div className="d-flex justify-content-center ">
        <div className="d-flex customs-btn-text">
          Số bản ghi: {number}/{records}
        </div>
        <button type="button" className="customs-btn-record mr-1" onClick={isFirst}>
          <img src={iconSrc.iFirstPage} height="18px" width="15px" alt="" />
        </button>
        <button type="button" className="customs-btn-record ml-1" onClick={isNext}>
          <img src={iconSrc.iLastPage} height="18px" width="15px" alt="" />
        </button>
      </div>
    );
  };
  return (
    <div className="form-search">
      <div className="form-wrapper">
        <div className="row">
          <div className={`col-12 col-lg-${withImage ? "10 col-xxl-11" : "12 col-xxl-12"} `}>
            {!titleNone && (
              <h4 className="d-flex justify-content-between">
                {title || "Điều kiện tìm kiếm"}
                {Number(records) > 1 && renderRecords()}
              </h4>
            )}

            {children}
          </div>
          {withImage && (
            <div className={`d-lg-block col-12 col-lg-2 col-xxl-1`}>
              <div className="img-dd" style={{ marginTop: 16, height: "auto" }}>
                <img
                  src={
                    withImage.length > 5 && typeof withImage === "string"
                      ? `data:image/png;base64,${withImage}`
                      : "/assets/icon/noun_not found_75231.svg"
                  }
                  alt=".."
                  style={{ objectFit: "contain", padding: "5px", maxWidth: "-webkit-fill-available" }}
                />
              </div>
            </div>
          )}
        </div>
      </div>
    </div>
  );
};
