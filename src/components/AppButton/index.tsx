import React from "react";
interface Props extends React.ButtonHTMLAttributes<HTMLButtonElement> {
  label?: string;
  icon?: any;
  img?: string;
  orange?: boolean;
  spanNumber?: string;
  blue?: boolean;
}
export default (props: Props) => {
  const { label, img, orange, spanNumber, type, blue, icon, ...remainProps } = props;
  return (
    <div className="in-block-Button">
      <div className="bg_in-Button">
        <button
          type={type || "submit"}
          className={`btn btn-primary ${orange && "btn-orange"} ${blue && "btn-blue"}`}
          {...remainProps}
        >
          {img && <img src={img} />}
          {icon && icon} &nbsp;
          {spanNumber && (
            <>
              <span className="shortcut-button">{spanNumber}</span>.{" "}
            </>
          )}
          {label}
        </button>
      </div>
    </div>
  );
};
