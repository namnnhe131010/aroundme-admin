import React from "react";
interface Props extends React.ButtonHTMLAttributes<HTMLButtonElement> {
  label?: string;
  img?: string;
  spanNumber?: string;
}
export default (props: Props) => {
  const { label, img, spanNumber, type, ...remainProps } = props;
  return (
    <div className="in-block-Button">
      <div className="bg_in-Button">
        <button type={type || "submit"} className={`btn btn-primary btn-yellow`} {...remainProps}>
          {img && <img src={img} />}
          <i className="fas fa-search" style={{ marginRight: 5, fontSize: 16 }}></i>
          {spanNumber && (
            <>
              <span className="shortcut-button">{spanNumber}</span>.{" "}
            </>
          )}
          {label || "Tìm kiếm"}
        </button>
      </div>
    </div>
  );
};
