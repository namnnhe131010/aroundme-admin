import React, { Fragment } from "react";
import AppInput from "components/AppInput";

interface Props {
  form?: any;
  field?: any;
  maxLength?: any;
  placeholder?: any;
  type?: any;
  label?: any;
  disabled?: any;
  onChangeCustomize?: any;
  onKeyDown?: any;
  style?: any;
  invalid?: any;
  className?: any;
  noLabel?: any;
  isRequired?: any;
  readOnly?: any;
  accept?: any;
}

const InputField = (props: Props) => {
  const {
    form,
    field,
    maxLength,
    placeholder,
    type,
    label,
    disabled,
    onChangeCustomize,
    onKeyDown,
    style,
    invalid,
    className,
    noLabel,
    isRequired,
    readOnly,
    accept,
  } = props;
  const { name } = field;
  const { errors, touched } = form;

  const onChange =(event:any)=>{
    console.log("name",field.name);
    form.setFieldValue(field.name,event.currentTarget.files[0])
  }

  return (
    <Fragment>
      {/* {label && (
        <label htmlFor={name} dangerouslySetInnerHTML={{ __html: label }} />
      )} */}
      <AppInput
        {...field}
        style={style}
        className={className}
        onChange={onChangeCustomize ||onChange}
        type={type}
        // id={name}
        maxLength={maxLength}
        value={null}
        placeholder={placeholder}
        disabled={disabled}
        invalid={invalid || (!!errors[name] && touched[name])}
        onKeyDown={onKeyDown}
        title={label}
        noLabel={noLabel}
        isRequired={isRequired}
        readOnly={readOnly}
        accept={accept}
      />
      {touched[name] && errors[name] && <div className="err-text">{errors[name]}</div>}
    </Fragment>
  );
};

InputField.defaultProps = {
  type: "text",
  tabIndex: "0",
  invalid: "false",
};

export default InputField;
