import React, { Fragment } from "react";
import AppInput from "components/AppInput";

interface Props {
    form?: any;
    field?: any;
    maxLength?: any;
    placeholder?: any;
    type?: any;
    label?: any;
    disabled?: any;
    onChangeCustomize?: any;
    onKeyDown?: any;
    style?: any;
    invalid?: any;
    className?: any;
    noLabel?: any;
    isRequired?: any;
    readOnly?: any;
    formatNumber?: boolean
}

const InputField = (props: Props) => {
  const {
    form,
    field,
    maxLength,
    placeholder,
    type,
    label,
    disabled,
    onChangeCustomize,
    onKeyDown,
    style,
    invalid,
    className,
    noLabel,
    isRequired,
    readOnly,
    formatNumber
  } = props;
  const { name, value } = field;
  const { errors, touched } = form;

  const format = (n: any) => {
    // format number 1000000 to 1,234,567
    return n.replace(/\D/g, "").replace(/\B(?=(\d{3})+(?!\d))/g, ",");
  };

  return (
    <Fragment>
      {/* {label && (
        <label htmlFor={name} dangerouslySetInnerHTML={{ __html: label }} />
      )} */}
      <AppInput
        {...field}
        style={style}
        className={className}
        onChange={onChangeCustomize || field.onChange}
        type={type}
        // id={name}
        maxLength={maxLength}
        value={formatNumber ? format(value.toString()) : value}
        placeholder={placeholder}
        disabled={disabled}
        invalid={invalid || (!!errors[name] && touched[name])}
        onKeyDown={onKeyDown}
        title={label}
        noLabel={noLabel}
        isRequired={isRequired}
        readOnly={readOnly}
      />
      {touched[name] && errors[name] && <div className="err-text">{errors[name]}</div>}
    </Fragment>
  );
};

InputField.defaultProps = {
  type: "text",
  tabIndex: "0",
  invalid: "false",
};

export default InputField;
