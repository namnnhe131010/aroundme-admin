import React from "react";

interface Props extends React.InputHTMLAttributes<HTMLInputElement> {
  title?: string;
  checkedValue?:any;
  field?: any;
  valueChecked?: any;
  style?: any;
  styleWrapper?: any;
  form?: any;
}

export default React.memo((props: Props) => {
  const { title, form, field, valueChecked, style, styleWrapper, checkedValue,...remainProps } = props;

  const str2bool = (value: any) => {
    if (value && typeof value === "string") {
      if (value.toLowerCase() === "true") return true;
      if (value.toLowerCase() === "false") return false;
    }
    return value;
  };

  return (
    <div className="checkbox-wrapper max-width-auto radioType">
      <label className="containCheck">
        <input {...field} value={str2bool(checkedValue)} checked={checkedValue && checkedValue === field.value} type={"radio"} {...remainProps} />
        <span className="checkmark" />
        <span className="checkbox-label">{title}</span>
      </label>
    </div>
  );
});
