import React, { useState } from "react";
import { Link } from "react-router-dom";
import { Dropdown, DropdownItem, DropdownMenu, DropdownToggle } from "reactstrap";

interface Props {
  value?: any;
  options?: any;
  placeholder?: string;
  onChange?: any;
}

const MenuCollapse = (props: Props) => {
  const { value, options = [], placeholder = "Select" } = props;

  const [dropdownOpen, setDropdownOpen] = useState(false);

  const toggle = () => setDropdownOpen((prevState) => !prevState);

  // const onMouseEnter = () => {
  //   setIsOpen(true);
  // };
  // const onMouseLeave = () => {
  //   setIsOpen(false);
  // };

  const urlPaths = window.location.hash;

  const checkUrlPathChildren = (urlLink: any) => {
    const urlPath = window.location.hash;
    const silceUrl = urlPath.replace("#", "");
    if (silceUrl && silceUrl === urlLink) {
      return "active";
    }
    return "";
  };
  const checkUrlParent = () => {
    const urlPath = window.location.hash;
    const silceUrl = urlPath.replace("#", "");
    if (urlPath && options?.children.some((i: any) => i.url === silceUrl.trim())) {
      return "active";
    }
    return "";
  };

  return (
    <Dropdown isOpen={dropdownOpen} toggle={toggle} className="text-left custome-drop-down ">
      <Link to="/" onClick={(e: any) => e.preventDefault()}>
        <DropdownToggle nav className={checkUrlParent()}>
          {value || placeholder}
        </DropdownToggle>
      </Link>

      {options.children.length > 0 && (
        <DropdownMenu className="custome-drop-down-item">
          {options.children &&
            options.children.map((opt: any, idx: any) => (
              <DropdownItem className="p-0 m-0" key={idx}>
                <Link className={`dropdown-item ${checkUrlPathChildren(opt.url)}`} to={`${opt.url}`} onClick={toggle}>
                  {opt.title}
                </Link>
              </DropdownItem>
            ))}
        </DropdownMenu>
      )}
    </Dropdown>
  );
};

export default MenuCollapse;
