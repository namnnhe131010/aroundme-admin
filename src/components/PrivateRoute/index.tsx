import { getDataToken } from "helpers/getLocalStore";
import isEmpty from 'lodash/isEmpty';
import React from "react";
import { Redirect, Route } from "react-router-dom";

const PrivateRoute = (props: any) => {
  const token: any = getDataToken();

  if (!isEmpty(token)) {
    return <Route {...props} />;
  }
  return <Redirect to={`/login`} />;
};

export default PrivateRoute;
