import MenuComponent from "components/MenuComponent";
import MenuCollapse from "components/Menu_collapse";
import { useWindowSize } from "helpers/customeHook";
import { getLocalMenu, getUserRole } from "helpers/getLocalStore";
import { isEmpty } from "lodash";
import React, { useEffect, useState } from "react";
import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import { Collapse, DropdownMenu, DropdownToggle, Nav, Navbar, NavbarToggler, UncontrolledDropdown } from "reactstrap";
import { logOutRequest } from "redux/modules/auth";
import { getUserRequest } from "redux/modules/user/actions";
import { GetAuthSelector, GetTypeAuth, GetUserInfo } from "redux/selectors/auth";
import httpServices from "services/httpServices";
import { Menu } from "constants/FixedMenu";

import logoTxt from "../../assets/images/logo_text_banner.svg";

const Header: React.FC<any> = (props) => {
  const sizeScreen = useWindowSize();
  const auth = GetAuthSelector();
  // const userInfoReducer = GetUserInfo();
  const actionType = GetTypeAuth();

  const dispatch = useDispatch();
  const localMenu = getLocalMenu();
  // const userRole = getUserRole();
  // const userInfo = getUserAll();

  const [menu, setMenu] = useState<any>(Menu);
  const [user, setUser] = useState<any>({});
  // const [menu, setMenu] = useState<any>(localMenu);
  // const [user, setUser] = useState<any>(userInfo);

  const [collapseMenu, setCollapseMenu] = useState(false);

  const [accountBox, setAccountBox] = useState(false);

  const toggleAccountBox = () => setAccountBox((prevState) => !prevState);

  useEffect(() => {
    // if (isEmpty(userInf)) {
    //   dispatch(getUserRequest());
    // }
  }, []);

  // useEffect(() => {
  //   if (isEmpty(userInfoReducer)) {
  //     setUser(getUserAll());
  //   } else {
  //     setUser(userInfoReducer);
  //   }
  //   setMenu(getLocalMenu());
  // }, [auth, actionType, userInfoReducer]);

  const signOut = () => {
    httpServices.removeInterceptors();
    httpServices.clearLocalStorage();
    httpServices.attachTokenToHeader();
    // window.reload();

    // dispatch(logOutRequest());
  };

  const renderMenu = () => {
    // if (menu && !(sizeScreen?.width < 800)) {
    //   return (
    //     <>
    //       <Nav id="ul-menu" className="mr-auto" navbar>
    //         {menu && menu.length > 4
    //           ? menu
    //               .slice(0, 5)
    //               .map((route: any, idx: any) => <MenuComponent key={idx} placeholder={route.title} options={route} />)
    //           : menu.map((route: any, idx: any) => <MenuComponent key={idx} placeholder={route.title} options={route} />)}
    //       </Nav>

    //       <UncontrolledDropdown>
    //         {menu && menu.length > 4 && (
    //           <DropdownToggle nav>
    //             <img src="/assets/images/menu_next.png" width="14px" height="16px" alt="" />
    //           </DropdownToggle>
    //         )}
    //         <DropdownMenu right>
    //           {menu &&
    //             menu.length > 4 &&
    //             menu
    //               .slice(5)
    //               .map((route: any, idx: any) => <MenuCollapse key={idx} placeholder={route.title} options={route} />)}
    //         </DropdownMenu>
    //       </UncontrolledDropdown>
    //     </>
    //   );
    // }
    return (
      <>
        <Nav id="ul-menu" className="mr-auto" navbar>
          {menu &&
            menu.map((route: any, idx: any) => <MenuComponent key={idx} placeholder={route.title} options={route} />)}
        </Nav>
      </>
    );
  };

  return (
    <header>
      <div className="container">
        <div className="header">
          <div className="logo-menu">
            <div className="d-none d-md-block logo">
              {/* <Link to="/">
                <img src={logoTxt} alt="" />
              </Link> */}
            </div>
            {/* 
            <div className="menu">
              <Navbar light expand="lg">
                <NavbarToggler onClick={() => setCollapseMenu(!collapseMenu)} />
                <Collapse isOpen={collapseMenu} navbar>
                  {renderMenu()}
                </Collapse>
              </Navbar>
            </div> */}
          </div>
          <div className="d-none d-md-block account" onClick={() => toggleAccountBox()}>
            <li className="btn-account">
              <img className={`${accountBox && "active"}`} src="/assets/images/icon_account.png" alt="" />
            </li>
            <ul className={`menu-account ${accountBox && "active"}`}>
              {/* <li>{user?.tenCanBo || "ten can bo"}</li>
              <li>
                <img src="/assets/images/diadiem.svg" width="10px" height="14px" alt="" />
                {"role name"}
              </li>
              <li>
                <Link to="/he-thong/thay-doi-mat-khau"> Đổi mật khẩu</Link>
              </li> */}
              <li>
                <a
                  onClick={() => {
                    signOut();
                  }}
                >
                  Log out
                </a>
              </li>
            </ul>
          </div>
        </div>
      </div>
    </header>
  );
};

export default Header;
