import React from "react";

interface Props {
  isvisible: boolean;
}

export default (props: Props) => {
  const { isvisible } = props;
  if (isvisible) {
    return (
      <div className="loading-container">
        <div className="lds-dual-ring"></div>
      </div>
    );
  }
  return null;
};
