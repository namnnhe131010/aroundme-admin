import React, { Fragment } from "react";

const AppTextArea = (props?: any) => {
  const {
    cols,
    rows,
    form,
    field,
    maxLength,
    placeholder,
    type,
    label,
    disabled,
    onChangeCustomize,
    onKeyDown,
    style,
    invalid,
    className,
    isRequired,
    readOnly,
    styleWrap,
    ...remainProps
  } = props;

  {
    if (field) {
      return (
        <Fragment>
          <div className="app-input-wrap" style={styleWrap}>
            {label && (
              <label>
                {label || ""} {isRequired && <span className="required">*</span>}
              </label>
            )}
            <textarea
              {...field}
              readOnly={readOnly}
              className={className}
              onChange={onChangeCustomize || field.onChange}
              type={type}
              id={field.name}
              maxLength={maxLength}
              value={field.value}
              placeholder={placeholder}
              disabled={disabled}
              invalid={invalid || (!!form.errors[field.name] && form.touched[field.name])}
              onKeyDown={onKeyDown}
              cols={cols}
              rows={rows}
              style={style}
            />
            {form.touched[field.name] && form.errors[field.name] && (
              <div className="err-text">{form.errors[field.name]}</div>
            )}
          </div>
        </Fragment>
      );
    } else {
      return (
        <Fragment>
          <div className="app-input-wrap" style={styleWrap}>
            {label && (
              <label>
                {label || ""} {isRequired && <span className="required">*</span>}
              </label>
            )}
            <textarea
              {...remainProps}
              readOnly={readOnly}
              className={className}
              onChange={onChangeCustomize}
              maxLength={maxLength}
              // value={value}
              placeholder={placeholder}
              disabled={disabled}
              onKeyDown={onKeyDown}
              cols={cols}
              rows={rows}
              style={style}
            />
          </div>
        </Fragment>
      );
    }
  }
};

AppTextArea.defaultProps = {
  type: "text",
  tabIndex: "0",
  invalid: "false",
};

export default AppTextArea;
