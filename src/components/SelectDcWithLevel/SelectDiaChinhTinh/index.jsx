/*
 * /api/danh-muc-dan-toc
 * danh mục đơn vị
 */
import { DiaChinh } from "constants/jsonDiaChinh";
import React, { memo, useEffect, useState } from "react";
import SelectComponents from "components/SelectComponents";

export default memo((props) => {
  const { title, defaultValue, form, field, disabled, functionProps, idTinh, ...remainProps } = props;
  const [data, setData] = useState(DiaChinh);
  const [loading] = useState(false);

  useEffect(() => {
    console.log("====================================");
    console.log(data);
    console.log("====================================");
  }, []);

  const selectedOption = field && data?.find((option) => option?.id + "" === `${field?.value}`);
  const selectedDefault = data?.find((option) => option?.values === defaultValue);

  const patchedOnChange = (selectedOption) => {
    const selectedValue = selectedOption ? selectedOption.id : selectedOption;

    const changeEvent = {
      target: {
        name: field.name,
        value: selectedValue,
      },
    };
    field.onChange(changeEvent);

    functionProps && functionProps(selectedOption);
  };

  const renderSelectType = () => {
    if (field) {
      return (
        <>
          <SelectComponents
            {...field}
            id={field.name}
            isLoading={loading}
            title={title}
            value={selectedOption || ""}
            onChange={patchedOnChange}
            options={data}
            getOptionLabel={(item) => `${item.ten}`}
            getOptionValue={(item) => item.id}
            error={form.touched[field.name] && form.errors[field.name]}
            {...remainProps}
            disabled={disabled}
          />
        </>
      );
    }

    return (
      <SelectComponents
        isLoading={loading}
        options={data}
        title={props.title}
        value={selectedDefault}
        getOptionLabel={(item) => `${item.ten}`}
        getOptionValue={(item) => item.id}
        {...remainProps}
      />
    );
  };
  return renderSelectType();
});
