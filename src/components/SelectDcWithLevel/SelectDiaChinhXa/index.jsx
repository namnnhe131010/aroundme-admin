/*
 * /api/danh-muc-dan-toc
 * danh mục đơn vị
 */
import { isEmpty } from "lodash-es";
import React, { memo, useEffect, useState } from "react";
import { CONDITIONS_DC_LV3 } from "../../../common/constanst";
import { DiaChinh } from "../../../common/data/jsonDiaChinh";
import { getUserAuth } from "../../../helpers/functions";
import SelectComponents from "../../SelectComponents";

export default memo((props) => {
  const {
    title,
    defaultValue,
    form,
    field,
    donViId,
    phongBanId,
    disabled,
    idTinh,
    idHuyen,
    functionProps,
    ...remainProps
  } = props;

  const [data, setData] = useState([]);
  const [loading] = useState(false);
  const userLever = getUserAuth();

  useEffect(() => {
    const { roleCodes, xaId } = userLever || {};

    if (idTinh && idHuyen) {
      const arrayXa = DiaChinh?.filter(
        (el, idx) => el?.id + "" === `${idTinh}`
      )[0].lstCon.filter((el, idx) => el?.id + "" === `${idHuyen}`);

      if (!isEmpty(arrayXa)) {
        if (
          (roleCodes && roleCodes.includes("ADMIN_TW")) ||
          roleCodes.includes("Tỉnh") ||
          roleCodes.includes("Huyện")
        ) {
          setData(arrayXa[0]?.lstCon || []);
          form && form.setFieldValue(field.name, "");
          return;
        }
        if (
          roleCodes &&
          CONDITIONS_DC_LV3.some((el) => roleCodes.includes(el))
        ) {
          setData(arrayXa[0]?.lstCon?.filter((item) => item.id == xaId) || []);
          form && form.setFieldValue(field.name, "");
          return;
        }
        setData(arrayXa[0]?.lstCon || []);
        form && form.setFieldValue(field.name, "");
      }
    } else {
      form && form.setFieldValue(field.name, "");
      setData([]);
    }
  }, [idTinh, idHuyen]);

  const selectedOption =
    field && data?.find((option) => option?.id === `${field?.value}`);
  const selectedDefault = data?.find(
    (option) => option?.values === defaultValue
  );

  const patchedOnChange = (selectedOption) => {
    //
    const selectedValue = selectedOption ? selectedOption.id : selectedOption;

    const changeEvent = {
      target: {
        name: field.name,
        value: selectedValue,
      },
    };
    field.onChange(changeEvent);

    functionProps && functionProps(selectedOption);
  };

  const renderSelectType = () => {
    if (field) {
      return (
        <>
          <SelectComponents
            {...field}
            id={field.name}
            isLoading={loading}
            title={title}
            value={selectedOption || ""}
            onChange={patchedOnChange}
            options={data}
            getOptionLabel={(item) => `${item.ten}`}
            getOptionValue={(item) => item.id}
            error={form.touched[field.name] && form.errors[field.name]}
            {...remainProps}
            disabled={disabled}
          />
        </>
      );
    }

    return (
      <SelectComponents
        isLoading={loading}
        options={data}
        title={props.title}
        value={selectedDefault}
        getOptionLabel={(item) => `${item.ten}`}
        getOptionValue={(item) => item.id}
        {...remainProps}
      />
    );
  };
  return renderSelectType();
});
