import { checkCallAPI } from "constants/api";
import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { Dropdown, DropdownItem, DropdownMenu, DropdownToggle } from "reactstrap";
// import { ghiLogHT } from "redux/modules/log_HT";
import httpServices from "services/httpServices";


interface Props {
  value?: any;
  options?: any;
  placeholder?: string;
  onChange?: any;
}

const MenuCP = (props: Props) => {
  const { value, options = [], placeholder = "Select" } = props;
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);

  const [dropdownOpen, setDropdownOpen] = useState(false);
  const toggleMenu = () => setDropdownOpen((prevState) => !prevState);

  const urlPaths = window.location.hash;

  const checkUrlPathChildren = (urlLink: any) => {
    // const urlPath = window.location.hash;
    const silceUrl = urlPaths.replace("#", "");
    if (silceUrl && silceUrl === urlLink) {
      return "active";
    }
    return "";
  };

  const checkUrlPathParent = () => {
    const urlPath = window.location.hash;
    const silceUrl = urlPath.replace("#", "");
    if (urlPath && options?.children.some((i: any) => i.url === silceUrl.trim())) {
      return true;
    }
    return false;
  };

  //
  const logSystem = React.useCallback(() => {
    const urlPath = window.location.hash;
    const silceUrl = urlPath.replace("#", "");
    if (silceUrl && options?.children.find((i: any) => i.url === silceUrl.trim())) {
      // find chuc nangw
      const value = options?.children.find((i: any) => i.url === silceUrl.trim());

      httpServices.attachFunctionCodeToHeader(value.idChucNang);

      // params  send
      const params = {
        chucNangId: value.key,
        maChucNang: value.idChucNang,
      };

      // ghiLogHT(params)
      //   .then((res: any) => {
      //     checkCallAPI(
      //       res,
      //       (response: any) => {
      //         // console.log("logging");
      //       },
      //       (e: any) => {
      //         console.log("logging F");
      //       }
      //     );
      //   })
      //   .catch((err: any) => {})
      //   .finally(() => {});
      return;
    }
    return "";
  }, []);

  useEffect(() => {
    logSystem();
  }, [urlPaths]);

  return (
    // <NavItem className="nav-item" active={checkUrlPathParent()}>
    <Dropdown active={checkUrlPathParent()} isOpen={dropdownOpen} toggle={toggle} nav>
      <DropdownToggle nav caret>
        {/* <Link to="/" className="nav-link" onClick={(e: any) => e.preventDefault()}> */}
        {value || placeholder}
        {/* </Link> */}
      </DropdownToggle>

      {options.children.length > 0 && (
        <DropdownMenu>
          <div className="bg-dropdown">
            {options.children &&
              options.children.map(
                (opt: any, idx: any) =>
                 (
                    <DropdownItem key={idx} className="p-0 bg-light">
                      <Link className={`dropdown-item ${checkUrlPathChildren(opt.url)}`} to={`${opt.url}`}>
                        {opt.title}
                      </Link>
                    </DropdownItem>
                  )
              )}
          </div>
        </DropdownMenu>
      )}
    </Dropdown>
    // </NavItem>
  );
};

export default MenuCP;
