import iconSrc from "helpers/iconSrc";
import React, { useEffect, useState } from "react";
import Select, { components } from "react-select";
import { createUltimatePagination } from "react-ultimate-pagination";

interface Option {
  value: number;
  label: string;
}

const arr: Option[] = [
  { value: 10, label: "10" },
  { value: 20, label: "20" },
  { value: 30, label: "30" },
  // { value: 40, label: "40" },
  // { value: 50, label: "50" },
  // { value: 60, label: "60" },
  // { value: 70, label: "70" },
  // { value: 80, label: "80" },
  // { value: 90, label: "90" },
  // { value: 100, label: "100" },
];

const customStyles = {
  // Nếu có ...provider thì sẽ mặc định style cũ
  control: (provided: any, state: any) => ({
    ...provided,
    // borderColor: "#fff",
    minHeight: "30px",
    height: "30px",
    boxShadow: state.isFocused ? null : null,
    // border: "none",
    // background: "#fff",
    width: "50px",
    // border: "1px"
    borderColor: "#d5d5d5",
  }),

  valueContainer: (provided: any, state: any) => ({
    ...provided,
    height: "30px",
    padding: "0 4px",
    textAlign: "center",
  }),
  menu: (provided: any, state: any) => ({
    ...provided,
    zIndex: 9999,
    textAlign: "center",
  }),
  input: (provided: any, state: any) => ({
    ...provided,
    margin: "0px",
    textAlign: "center",
  }),
  indicatorSeparator: (provided: any, state: any) => ({
    // Thanh phân cách giữa input và nút xuống
    display: "none",
  }),
  indicatorsContainer: (provided: any, state: any) => ({
    ...provided,
    height: "30px",
  }),
  dropdownIndicator: (state: any) => ({
    // Mũi tên trỏ xuống
    // display: dropDownIcon ? "none" : "block",
    margin: "5px",
  }),
  loadingIndicator: (provided: any, state: any) => ({
    // loading style
    ...provided,
  }),
};

interface Props {
  totalPages?: number;
  select?: boolean;
  onChangeSize?: (value: Option) => void;
  onChangePage?: (page: number) => void;
  currentPage?: number;
  total?: any;
  currentSize?: number;
  totalHandle?: any;
  titleTotal?: any;
  titleHandle?: any;
  cantChangeSize?: boolean
}

export default (props: Props) => {
  const {
    totalPages,
    select,
    onChangeSize,
    onChangePage,
    total,
    currentSize,
    totalHandle,
    titleTotal,
    titleHandle,
    cantChangeSize
  } = props;
  const findSize = () => {
    return arr.find((elm: Option) => elm.value === Number(currentSize)) || arr[0];
  };

  const [page, setPage] = useState(1);
  const [pageSize, setSize] = useState(currentSize ? findSize() : arr[0]);
  const [dropdownOpen, setDropdownOpen] = useState(false);

  const toggle = () => setDropdownOpen((prevState) => !prevState);

  const onChangePageState = (currentPage: number) => {
    setPage(currentPage);
    !cantChangeSize && onChangePage && onChangePage(currentPage);
  };

  const DropdownIndicator = (props: any) => {
    return (
      <components.DropdownIndicator {...props}>
        <img src="/assets/images/arrowdown.svg" width="7px" alt="" />
      </components.DropdownIndicator>
    );
  };

  useEffect(() => {
    if (currentSize) {
      setSize(findSize());
    }
  }, [currentSize]);

  const onChangePageSize = (value: any) => {
    onChangePageState(1);
    onChangeSize && onChangeSize(value as Option);
    setSize(value);
  };

  const dropDown = () => {
    return (
      <div className="page-dropdow">
        <span style={{ color: "rgb(77, 78, 78)", marginRight: 4 }}>Hiển thị</span>
        <Select
          aria-labelledby="select-size"
          aria-label="select-size"
          styles={customStyles}
          placeholder=""
          isClearable={false}
          options={arr || []}
          components={{ DropdownIndicator }}
          defaultValue={arr[0]}
          isSearchable={false}
          // getOptionLabel={(item) => `Hiển thị ${item.value}`}
          // onChange={(value) => {
          //   onChangePageState(1);
          //   onChangeSize && onChangeSize(value as Option);
          // }}
          value={pageSize}
          onChange={onChangePageSize}
        />
      </div>
    );
  };

  function Page(props: any) {
    return (
      <button
        type="button"
        style={props.isActive ? { background: "#4D4E4E", color: "#fff" } : { background: "#fff", color: "#4D4E4E" }}
        onClick={props.onClick}
        disabled={props.disabled}
        className="customs-btn-border"
      >
        {props.value}
      </button>
    );
  }

  function Ellipsis(props: any) {
    return (
      <button type="button" onClick={props.onClick} disabled={props.disabled} className="customs-btn-border">
        ...
      </button>
    );
  }

  function FirstPageLink(props: any) {
    return (
      <button
        onClick={props.onClick}
        disabled={page === 1 ? true : false}
        style={{ borderBottomLeftRadius: "5px", borderTopLeftRadius: "5px" }}
        className="customs-btn-border"
      >
        <img src={iconSrc.iFirstPage} height="10px" width="10px" alt="" />
      </button>
    );
  }

  function PreviousPageLink(props: any) {
    return (
      <button
        onClick={props.onClick}
        disabled={page === 1 ? true : false}
        className="customs-btn-disabled customs-btn-border"
      >
        <img src={iconSrc.iBackPage} height="10px" width="10px" alt="" />
      </button>
    );
  }

  function NextPageLink(props: any) {
    return (
      <button
        onClick={props.onClick}
        disabled={page === totalPages ? true : false}
        className="customs-btn-disabled customs-btn-border"
      >
        <img src={iconSrc.iNextPage} height="10px" width="10px" alt="" />
      </button>
    );
  }

  function LastPageLink(props: any) {
    return (
      <button
        onClick={props.onClick}
        disabled={page === totalPages ? true : false}
        className="customs-btn-disabled customs-btn-border"
        style={{ borderTopRightRadius: "5px", borderBottomRightRadius: "5px" }}
      >
        <img src={iconSrc.iLastPage} height="10px" width="10px" alt="" />
      </button>
    );
  }

  function Wrapper(props: any) {
    return <div className="pagination">{props.children}</div>;
  }
  const PaginatedPage = createUltimatePagination({
    itemTypeToComponent: {
      PAGE: Page,
      ELLIPSIS: Ellipsis,
      FIRST_PAGE_LINK: FirstPageLink,
      PREVIOUS_PAGE_LINK: PreviousPageLink,
      NEXT_PAGE_LINK: NextPageLink,
      LAST_PAGE_LINK: LastPageLink,
    },
    WrapperComponent: Wrapper,
  });

  return (
    // <div className="d-flex col-12 justify-content-between pl-3 pr-3 pt-3 pb-3">
    //   <div className="col-12 col-sm-3 d-flex justify-content-start ">{select && dropDown()}</div>
    //   <div className="col-12 col-sm-3">
    //     {total && <AppInput title="Tổng số danh sách" horizontal marginLeft value={total} readOnly />}
    //   </div>
    //   <div className="col-12 col-sm-3 d-flex justify-content-end">
    //     {Number(totalPages) > 1 && (
    //       <PaginatedPage
    //         totalPages={totalPages || 100}
    //         currentPage={props.currentPage || page}
    //         onChange={onChangePageState}
    //       />
    //     )}
    //   </div>
    // </div>
    <div className="justify-content-between m-0 p-0 py-2 row">
      <div className="col-12 col-md-3 p- d-flex justify-content-start align-items-center">{select && dropDown()}</div>

      <div className="col-12 col-md-3 d-flex align-items-center ">
        {total && (
          <span>
            {titleTotal ? titleTotal : "Tổng số danh sách:"} <span className="border rounded p-1">{total}</span>
          </span>
        )}
      </div>
      <div className="col-12 col-md-3 d-flex  align-items-center">
        {typeof totalHandle != "undefined" && (
          <span>
            {titleHandle ? titleHandle : "Số danh sách được chọn: "}
            <span className="border rounded p-2">{totalHandle}</span>
          </span>
        )}
      </div>

      {Number(totalPages) > 1 && (
        <div className="col-12 col-md-3 d-flex justify-content-end align-items-center">
          <PaginatedPage
            totalPages={totalPages || 100}
            currentPage={props.currentPage || page}
            onChange={onChangePageState}
          />
        </div>
      )}
    </div>
  );
};
