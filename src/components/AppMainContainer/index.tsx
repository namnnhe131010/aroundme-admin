import React from "react";
import img from "assets/images/icon_tieu de.svg";
interface Props {
  title?: string;
  children: any;
  style?: React.CSSProperties | undefined;
  contentStyle?: React.CSSProperties | undefined;
}

export default (props: Props) => {
  const { title, children, style, contentStyle } = props;
  return (
    <div className="container" style={style}>
      {title && (
        <div className="d-flex">
          <img alt="" src={img} width="20" />
          <h1 className="title-canhan">{title}</h1>
        </div>
      )}
      <div id="right-content" className="right-content" style={contentStyle}>
        {children}
      </div>
    </div>
  );
};
