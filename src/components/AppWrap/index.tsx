import React from "react";

interface Props {
  children?: any;
  title?: string;
}

export default (props: Props) => {
  return (
    <div className="app-wrap-component">
      {props.title && <h4>{props.title}</h4>}
      <div>{props.children}</div>
    </div>
  );
};
