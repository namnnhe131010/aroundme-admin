import React from 'react';

export default interface StyleSheet {
  [key: string]: React.CSSProperties;
}
