export interface User {
  activated?: boolean;
  authorities?: [];
  createdBy?: string;
  createdDate?: string;
  email?: string;
  firstName?: string;
  id?: string | number;
  imageUrl?: string;
  langKey?: string;
  lastModifiedBy?: string;
  lastModifiedDate?: string;
  lastName?: string;
  login?: string;
}
