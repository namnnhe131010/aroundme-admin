enum iconSrc {
  iQuestion = "/assets/icon/icon_question.svg",
  iSuccess = "/assets/icon/icon_success.svg",
  iWarning = "/assets/icon/icon_warning.svg",
  iSuccessWhite = "/assets/icon/icon_success_white.svg",
  iBackPage = "/assets/icon/icon_back_page.svg",
  iNextPage = "/assets/icon/icon_next_page.svg",
  iLastPage = "/assets/icon/icon_last_page.svg",
  iFirstPage = "/assets/icon/icon_first_page.svg",
  iDelete = "/assets/icon/icon_delete.svg",
  iEdit = "/assets/icon/icon_edit.svg",
  iFolder = "/assets/icon/folder.svg",
  iCancel = "/assets/icon/icon_cancel.svg",
  iSave = "/assets/icon/save.svg"
}

export default iconSrc;
