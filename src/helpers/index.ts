export const TYPE = {
  1: "1",
  2: "2",
};

export const DATE_FORMAT = "DD-MM-YYYY";

export const numberToCurrency = (number: any) => {
  if (!number) {
    return 0;
  }
  return `${parseInt(number)
    .toString()
    .replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,")}`;
};
