import { uniqBy, isEmpty } from "lodash";

export const isAllItemOnPageChecked = (parent: Array<any>, child: Array<any>, key: any) => {
  return !isEmpty(child) && child.every((elm) => parent.findIndex((i) => i[key] === elm[key]) > -1);
};

export const removeCheckAllItems = (parent: Array<any>, child: Array<any>, key: any) => {
  return parent.filter((elm) => child.findIndex((i) => i[key] === elm[key]) < 0);
};

export const addAllItemOfPage = (targetArray: Array<any>, arrayForAdd: Array<any>, key: any) => {
  return uniqBy([...arrayForAdd, ...targetArray], key);
};

export const addAnItems = (targetArray: Array<any>, itemForAdd: any, key: any) => {
  const arrayAfterFilterItem = targetArray.filter((elm: any) => elm[key] !== itemForAdd[key]);
  if (arrayAfterFilterItem.length === targetArray.length) {
    arrayAfterFilterItem.push(itemForAdd);
  }
  return arrayAfterFilterItem;
};

export const isItemChecked = (parent: Array<any>, itemForCheck: any, key: any) => {
  return Boolean(parent.filter((elm: any) => elm[key] === itemForCheck[key]).length > 0);
};

export const selectedItem = (parent: Array<any>, itemForSelect: any, key: any) => {
  return uniqBy([...parent, ...[itemForSelect]], key);
};
