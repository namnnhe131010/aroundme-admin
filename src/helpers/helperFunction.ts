import { DATE_FORMAT } from "helpers";
import { cloneDeep, isEmpty, isEqual, remove } from "lodash";
import moment from "moment";

// export function handleCheckAll(name: string, checked: boolean) {
//   let listCheckbox: any = document.getElementsByName(name);
//   for (let l of listCheckbox) {
//     l.checked = checked;
//   }
// }

export const handleCheckAll = (name: string, checked: boolean) => {
  let listCheckbox: any = document.getElementsByName(name);
  for (let l of listCheckbox) {
    l.checked = checked;
  }
};

export function convertToQuery(param: any) {
  return (
    "?" +
    Object.keys(param)
      .map(function (key) {
        return encodeURIComponent(key) + "=" + encodeURIComponent(param[key]);
      })
      .join("&")
  );
}

export function checkKeyNull(obj: any) {
  for (let v in obj) {
    // if (typeof obj[v] !== "boolean") {
    //   if (isEmpty(obj[v])) {
    //     delete obj[v];
    //   } else if (typeof obj[v] == "string") obj[v] = obj[v].trim();
    // }
    switch (typeof obj[v]) {
      case "number":
        if (isNaN(obj[v]) || obj[v] == null) delete obj[v];
        break;
      case "string":
        if (isEmpty(obj[v])) delete obj[v];
        else obj[v] = obj[v].trim();
        break;
      default:
        if (obj[v] == null) delete obj[v];
        break;
    }
  }
  return obj;
}

export function checkKeyNull2(obj: any) {
  const cloneObj = cloneDeep(obj);
  for (let v in cloneObj) {
    if (typeof cloneObj[v] !== "boolean") {
      if (isEmpty(cloneObj[v])) {
        delete cloneObj[v];
      } else if (typeof cloneObj[v] == "string") cloneObj[v] = cloneObj[v].trim();
    }
  }
  return cloneObj;
}

export function processFieldDate(setFieldValue: any, fieldName: string, value: any) {
  if (value && moment(value, DATE_FORMAT).isValid()) {
    return setFieldValue(fieldName, moment(value, DATE_FORMAT).format(DATE_FORMAT));
  } else {
    return setFieldValue(fieldName, "");
  }
}

export function processFieldDateEpicRaw(setFieldValue: any, fieldName: string, value: any) {
  if (value && value.length === 10 && !value.includes("_")) {
    if (moment(value, DATE_FORMAT).isValid()) {
      return setFieldValue(fieldName, moment(value, DATE_FORMAT).format(DATE_FORMAT));
    } else {
      return setFieldValue(fieldName, "");
    }
  }
  return value;
}

export function handleCheckbox(event: any, checkedItems: any, item: any) {
  let cloneCheckedList = cloneDeep(checkedItems);
  event.target.checked ? cloneCheckedList.push(item) : remove(cloneCheckedList, (c) => isEqual(item, c));

  return cloneCheckedList;
}

export function calcItemStart(page: any = 1, size: any = 10) {
  return (page - 1) * size + 1;
}

export function convertQSToObj(querystring: string) {
  const params: any = new URLSearchParams(querystring);
  const obj: any = {};

  for (const key of params.keys()) {
    if (params.getAll(key).length > 1) {
      obj[key] = params.getAll(key);
    } else {
      obj[key] = params.get(key);
    }
  }

  return obj;
}

export const allowNumberOnly = (value: string) => {
  const re = /^[0-9\b]+$/;
  if (value === "" || re.test(value)) {
    return true;
  }
  return false;
};

export const trimmedObject = (obj: object) => {
  const trimed = JSON.stringify(obj, (key, value) => {
    if (typeof value === "string") {
      return value.trim();
    }
    return value;
  });
  return JSON.parse(trimed);
};
