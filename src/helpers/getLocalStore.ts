interface DanhSachQuyen {
  detail: any;
  donViId: any;
  macDinh: boolean;
  roleCode: any;
  roleId: number;
  roleName: string;
}


export const getDataToken =  () => {
  const data: any = localStorage.getItem("token") || "";
  return data ? JSON.parse(data): {} 
}

export const getLocalMenu = () => {
  const user: any = localStorage.getItem("user") || "";
  const menu: any = (user && JSON.parse(user)?.res?.danhSachChucNang) || [];
  return menu;
};

export const getUserRole = () => {
  const user: any = localStorage.getItem("user") || "";
  const role: any = (user && JSON.parse(user)?.res?.danhSachQuyen[0]) || [];
  return role;
};
