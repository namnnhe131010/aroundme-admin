import * as Yup from "yup";
import moment from "moment";

export const validateLoginSchema = Yup.object().shape({
  username: Yup.string().required("Enter username"),
  password: Yup.string().required('Enter password'),
});

export const changePasswordSchema = Yup.object().shape({
  currentPassword: Yup.string().required("Đây là trường bắt buộc").min(4, "Sai quy tắc mật khẩu"),
  newPassword: Yup.string().required("Đây là trường bắt buộc").min(4, "Sai quy tắc mật khẩu"),
  rePassword: Yup.string()
    .required("Đây là trường bắt buộc")
    .min(4, "Sai quy tắc mật khẩu")
    .oneOf([Yup.ref("newPassword")], "Nhập lại mật khẩu không trùng với mật khẩu"),
});

export const dateValid = (name: string) => {
  return Yup.string()
    .required("Đây là trường bắt buộc")
    .test(name, "Định dạng ngày tháng không chính xác", (value) => !moment(value, "DD-MM-YYYY").isValid);
};

export const textArea500Valid = () => {
  return Yup.string().required("Đây là trường bắt buộc").max(500, "Tối đa 500 kí tự");
};

export const textBox50Valid = () => {
  return Yup.string().required("Đây là trường bắt buộc").max(50, "Tối đa 50 kí tự");
};

export const requireFieldValid = () => {
  return Yup.string().required("Đây là trường bắt buộc");
};
export const requireDvValid = () => {
  return Yup.string().nullable().required("Bạn chưa chọn đơn vị");
};
export const requirePhamViTimKiemValid=()=>{
  return Yup.string().nullable().required("Bạn chưa chọn phạm vi tìm kiếm");
}