import { AxiosResponse } from "axios";
import { URL as URLS } from "./urls";
export const URL = URLS;
export const LOGIN = `${URL}authenticate`;
export const LOGIN_IDP = `https://vneid.teca.vn/auth/realms/officer/protocol/openid-connect/token`;
export const ACCOUNT = `${URL}account`;
export const CHANGE_PASSWORD = `${URL}account/change-password`;
export const CLIENT_ID = "asm-admin";

export const MENU = {
  MENU_CHUC_NANG: `${URL}danh-sach-chuc-nang`,
};

interface ErrorCall {
  data: any;
statusText: string;
}

const SUCCESS_STATUS = [200, 201, 202, 203, 204];
export const checkCallAPI = (
  response: AxiosResponse | any,
  onSuccess: (data: any) => void,
  onError: (data: ErrorCall) => void
) => {
  if (response?.status && SUCCESS_STATUS.includes(response!.status)) {
    onSuccess(response!.data);
    return;
  }
  onError({ data: response?.data, statusText: response?.statusText });
};

interface dataProps {
  code: any;
  message: string;
}

export const callAPI = (
  response: AxiosResponse | any,
  onSuccess: (data: any) => void,
  onError: (data: ErrorCall) => void
) => {
  if (response?.status && SUCCESS_STATUS.includes(response!.status)) {
    const returnData: dataProps = response!.data;
    if (response?.data?.code === 210) {
      returnData.message = "Hồ sơ đã được in";
    } else {
      returnData.message = "Thao tác thành công";
    }
    onSuccess(returnData);
  }
  onError({ data: response?.data, statusText: response?.statusText });
};

interface ReturnData {
  data: any;
  meta: {
    totalPage: number;
    totalItems: number;
  };
}

interface CallAPIProps {
  response: AxiosResponse | any;
  onSuccess: (data: ReturnData) => void;
  onError: (data: ErrorCall) => void;
  size: number | string;
}

export const callAPIPaging = (props: CallAPIProps) => {
  const { response, onSuccess, onError, size: limit } = props;
  if (SUCCESS_STATUS.includes(response!.status)) {
    const returnData: ReturnData = {
      data: response!.data,
      meta: { totalPage: 0, totalItems: 0 },
    };
    if (response.headers["x-total-count"]) {
      const allDataCount = response.headers["x-total-count"];
      returnData.meta.totalItems = allDataCount;
      if (Number(allDataCount) / Number(limit) > Math.round(Number(allDataCount) / Number(limit))) {
        returnData.meta.totalPage = Math.round(Number(allDataCount) / Number(limit)) + 1;
      } else {
        returnData.meta.totalPage = Math.round(Number(allDataCount) / Number(limit));
      }
    }
    onSuccess(returnData);
    return;
  }
  onError({ data: response?.data, statusText: response?.statusText });
};
