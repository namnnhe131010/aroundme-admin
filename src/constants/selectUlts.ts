export const convertToFormSelect = (
  list: any,
  fieldForLabel: any,
  fieldForValue: any
) => {
  return [
    ...list.reduce((arr: any, el: any) => {
      return [
        ...arr,
        {
          ...el,
          label: el[fieldForLabel] || "None",
          value: el[fieldForValue] || "",
        },
      ];
    }, []),
  ];
};
