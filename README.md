<h1 align="center">React + Redux + Redux Saga + Scss + Formik + Typescript</h1>

<p align="center">
  <a href="https://reactjs.org/" target="_blank"><img src="https://img.shields.io/badge/React-v16.13.1-%238DD6F9.svg?logo=React"></a>
  <a href="https://github.com/donezombie" target="_blank"><img src="https://img.shields.io/badge/licence-MIT-green.svg" /></a>
  <a href="https://www.typescriptlang.org/" target="_blank"><img src="https://badgen.net/badge/Built%20With/TypeScript/blue" /></a>
</p>

## Packges use in template:

- TypeScript
- React.js
- React-router-dom
- SCSS

- Redux
- Redux Saga

- Axios
- Formik
- Yup Validator
- Lodash
- Immer

## How to use

```javascript
npm i
npm start ( developepment )
npm run build ( production )
```

## Environment

You can access your .env variables by deconstructing 'process.env' object, both on client and server.
Just make sure that you reboot the server when updating .env file

### Development
- Restructure Redux folder. 